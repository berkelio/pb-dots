-- Functions that you use more than once and in different files would be nice to define here.

local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local wibox = require("wibox")
local naughty = require("naughty")

local helpers = {}

helpers.create_tooltip = function(tooltip_message)
    local mytooltip = awful.tooltip {
        shape = helpers.rrect(6),
        gaps = 10,
        text = tooltip_message,
        margins_leftright = 10,
        margins_topbottom = 10,
        preferred_alignments = {"middle", "front", "back"},
        mode = "outside",
    }     
end

helpers.create_button = function (symbol, color, bg_color, hover_color)
    local widget = wibox.widget {
        font = beautiful.font_icon .. " 8",
        align = "center",
        valign = "center",
        id = "text_role",
        markup = helpers.colorize_text(symbol, color),
        widget = wibox.widget.textbox(),
    }

    local section = wibox.widget {
        widget,
        bg = bg_color,
        forced_width = dpi(30),
        widget = wibox.container.background
        }
        

    -- Hover animation
    section:connect_signal("mouse::enter", function ()
        section.bg = hover_color
    end)
    section:connect_signal("mouse::leave", function ()
        section.bg = bg_color
    end)

   helpers.add_hover_cursor(section, "hand1")

    return section
end

helpers.create_sidebar_button = function (symbol, color, bg_color, hover_color, size)
    local widget = wibox.widget {
        font = beautiful.font_icon .. " 10",
        align = "center",
        valign = "center",
        id = "text_role",
        markup = helpers.colorize_text(symbol, color),
        widget = wibox.widget.textbox(),
    }

    local section = wibox.widget {
        {
            widget,
            left = dpi(0),
            right = dpi(0),
            widget = wibox.container.margin
        },
        bg = bg_color,
        shape = helpers.rrect(dpi(20)),
        forced_width = dpi(size),
        forced_height = dpi(size),
        widget = wibox.container.background
        }

    section:connect_signal("mouse::enter", function () section.bg = hover_color end)
    section:connect_signal("mouse::leave", function () section.bg = bg_color end)
    helpers.add_hover_cursor(section, "hand1")
    return section
end

helpers.create_sidebar_bar = function (icon, icon_color, bar_color)
    local icon = wibox.widget {
        font = beautiful.font_icon .. " 12",
        align = "center",
        valign = "center",
        id = "text_role",
        markup = helpers.colorize_text(icon, icon_color),
        widget = wibox.widget.textbox(),
    }

    local bar = wibox.widget {
        max_value           = 100,
        value               = 92,
        forced_height       = dpi(5),
        color               = bar_color,
        background_color    = bar_color.."50",
        visible             = true,
        margins = {bottom = dpi(5) ,top = dpi(5)},
        bar_shape           = helpers.rrect(8),
        shape               = helpers.rrect(8),
        widget              = wibox.widget.progressbar,
    }
        
    local widget = wibox.widget {
        icon,
        nil,
        {
            bar,
            widget = wibox.container.margin,
        },
        spacing = dpi(20),
        expand = "none",
        layout = wibox.layout.fixed.horizontal,
    }
    
    helpers.add_hover_cursor(widget, "hand1")
    return widget
end

-- Escapes a string so that it can be displayed inside pango markup
-- tags. Modified from:
-- https://github.com/kernelsauce/turbo/blob/master/turbo/escape.lua
function helpers.pango_escape(s)
    return (string.gsub(s, "[&<>]", {
        ["&"] = "&amp;",
        ["<"] = "&lt;",
        [">"] = "&gt;"
    }))
end

helpers.colorize_text = function(text, color)
    return "<span foreground='"..color.."'>"..text.."</span>"
end

function helpers.add_hover_cursor(w, hover_cursor)
    local original_cursor = "left_ptr"

    w:connect_signal("mouse::enter", function ()
        local w = _G.mouse.current_wibox
        if w then
            w.cursor = hover_cursor
        end
    end)

    w:connect_signal("mouse::leave", function ()
        local w = _G.mouse.current_wibox
        if w then
            w.cursor = original_cursor
        end
    end)
end

function helpers.volume_control(action)
    local cmd
    if action == "toggle" then
        cmd = "pactl set-sink-mute @DEFAULT_SINK@ toggle"
    elseif action == "up" then
        cmd = "pactl set-sink-mute @DEFAULT_SINK@ 0 && amixer -D pulse sset Master 5%+"
    else
        cmd = "pactl set-sink-mute @DEFAULT_SINK@ 0 && amixer -D pulse sset Master 5%-"
    end
    awful.spawn.with_shell(cmd)
end

function helpers.vertical_pad(height)
    return wibox.widget{
        forced_height = height,
        layout = wibox.layout.fixed.vertical
    }
end

function helpers.horizontal_pad(width)
    return wibox.widget{
        forced_width = width,
        layout = wibox.layout.fixed.horizontal
    }
end

local double_tap_timer = nil
function helpers.single_double_tap(single_tap_function, double_tap_function)
    if double_tap_timer then
        double_tap_timer:stop()
        double_tap_timer = nil
        double_tap_function()
        -- naughty.notify({text = "We got a double tap"})
        return
    end

    double_tap_timer =
        gears.timer.start_new(0.20, function()
            double_tap_timer = nil
            -- naughty.notify({text = "We got a single tap"})
            if single_tap_function then
                single_tap_function()
            end
            return false
        end)
end

function helpers.float_and_resize(c, width, height)
    c.maximized = false
    c.width = width
    c.height = height
    awful.placement.centered(c,{honor_workarea=true, honor_padding = true})
    awful.client.property.set(c, 'floating_geometry', c:geometry())
    c.floating = true
    c:raise()
end

function helpers.run_or_raise(match, move, spawn_cmd, spawn_args)
    local matcher = function (c)
        return awful.rules.match(c, match)
    end

    -- Find and raise
    local found = false
    for c in awful.client.iterate(matcher) do
        found = true
        c.minimized = false
        if move then
            c:move_to_tag(mouse.screen.selected_tag)
            client.focus = c
        else
            c:jump_to()
        end
        break
    end

    -- Spawn if not found
    if not found then
        awful.spawn(spawn_cmd, spawn_args)
    end
end

-- Create rounded rectangle shape (in one line)
helpers.rrect = function(radius)
    return function(cr, width, height)
        gears.shape.rounded_rect(cr, width, height, radius)
    end
end

-- Adds a maximized mask to a screen
function helpers.screen_mask(s, bg)
    local mask = wibox({visible = false, ontop = true, type = "splash", screen = s})
    awful.placement.maximize(mask)
    mask.bg = bg
    return mask
end

helpers.view_next_tag_with_client = function()
	local initial_tag_index = awful.screen.focused().selected_tag.index
	while (true) do
		awful.tag.viewnext()
		local current_tag = awful.screen.focused().selected_tag
		local current_tag_index = current_tag.index
		if #current_tag:clients() > 0 or current_tag_index == initial_tag_index then
			return
		end
	end
end

helpers.view_prev_tag_with_client = function()
	local initial_tag_index = awful.screen.focused().selected_tag.index
	while (true) do
		awful.tag.viewprev()
		local current_tag = awful.screen.focused().selected_tag
		local current_tag_index = current_tag.index
		if #current_tag:clients() > 0 or current_tag_index == initial_tag_index then
			return
		end
	end
end

return helpers
