#!/bin/sh

if [ $(bluetoothctl show | grep "Powered: yes" | wc -c) -eq 0 ]; then
  echo "apagado"
else 
    if [ $(echo info | bluetoothctl | grep 'Device' | wc -c) -eq 0 ]; then 
        echo "encendido"
    else 
        name=$(echo info | bluetoothctl | grep -i name | cut -d ":" -f2)
        echo "conectado - $name"
    fi
fi
