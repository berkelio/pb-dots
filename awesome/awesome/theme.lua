local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local theme_name = "amarena"
local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local gfs = require("gears.filesystem")
local helpers = require("helpers")

--local xrdb = xresources.get_current_theme()

local theme = {}

local screen_width = awful.screen.focused().geometry.width
local screen_height = awful.screen.focused().geometry.height

-- Set theme wallpaper.
theme.wallpaper = "~/.config/awesome/wallpaper.png"

-- Set the theme font. This is the font that will be used by default in menus, bars, titlebars etc.
theme.font = "Fira Sans"
theme.font_icon = "Material Icons"
theme.font_mono = "Fira Code"

-- This is how to get other .Xresources values (beyond colors 0-15, or custom variables)
-- local cool_color = awesome.xrdb_get_value("", "color16")

theme.bg_dark               = x.background
theme.bg_normal             = x.background
theme.bg_focus              = x.color8
theme.bg_urgent             = x.color8
theme.bg_minimize           = x.color8

--Tooltip configuration
theme.tooltip_bg = "#000000cc"
theme.tooltip_fg = x.foreground
theme.tooltip_font = theme.font .. " 10"
theme.tooltip_shape = helpers.rrect(6)

--Tray and traybox configuration
theme.bg_systray = "#111111"
theme.systray_icon_spacing = dpi(5)
theme.traybox_bg = theme.bg_systray
theme.traybox_height = dpi(40)

theme.fg_normal     = x.color8
theme.fg_focus      = x.color4
theme.fg_urgent     = x.color9
theme.fg_minimize   = x.color8

-- s
theme.useless_gap   = dpi(4)
theme.screen_margin = dpi(2)

-- Borders
theme.border_width  = dpi(1.2)
theme.border_color = "#000000"
theme.border_normal = theme.border_color
theme.border_focus  = theme.border_color
theme.border_radius = dpi(0)
theme.client_radius = dpi(0)

-- Widgets
theme.hover_color = x.color4.."90"
theme.widget_bg = '#000000' .. 'cc'
theme.widget_enabled_color = x.color15
theme.widget_disabled_color = "#666666"
theme.widget_icon_font = "Material Icons"

-- Titlebar
theme.titlebars_enabled = true
theme.titlebar_size = dpi(22)
theme.titlebar_border_size = dpi(1)
theme.titlebar_title_enabled = true
theme.titlebar_font = theme.font .. " medium 8"
theme.titlebar_title_align = "center"
theme.titlebar_position = "top"
theme.titlebar_bg = "#333"
theme.titlebar_bg_focus = x.color1
theme.titlebar_fg_focus = x.foreground
theme.titlebar_fg_normal = "#00000000"

-- Notifications
-- Position: bottom_left, bottom_right, bottom_middle, top_left, top_right, top_middle
theme.notification_position = "bottom_right"
theme.notification_border_width = dpi(1)
theme.notification_border_radius = dpi(12)
theme.notification_border_color = "#000000"
theme.notification_bg = x.color0
theme.notification_text_bg = x.background
theme.notification_icon_bg = theme.notification_text_bg
theme.notification_fg = x.foreground
theme.notification_crit_bg = x.background
theme.notification_crit_fg = x.color1
theme.notification_max_height = dpi(220)
theme.notification_margin = dpi(15)
theme.notification_font = theme.font .. " 9"
theme.notification_spacing = theme.screen_margin * 2

theme.pop_size = dpi(140)

-- Edge snap
theme.snap_shape = gears.shape.rectangle
theme.snap_bg = x.foreground
theme.snap_border_width = dpi(3)

-- Widget separator
theme.separator_text = "|"
theme.separator_fg = x.color8

-- Wibar (s)
theme.wibar_position = "bottom"
theme.wibar_width = screen_width
theme.wibar_height = dpi(24)
theme.wibar_fg = x.foreground
theme.wibar_bg = x.background
theme.wibar_x = screen_width-dpi(230)
theme.wibar_y = 0
theme.wibar_border_color = "#00000000"
theme.wibar_border_width = dpi(0)
theme.wibar_border_radius = dpi(0)
theme.prefix_fg = x.color8

-- Icons
theme.icon_folder = "~/.config/awesome/theme/icons/"
theme.icon_volmuted = theme.icon_folder.."volmuted.svg"
theme.icon_vol = theme.icon_folder.."vol.svg"
theme.icon_mic = theme.icon_folder.."mic.svg"
theme.icon_micmuted = theme.icon_folder.."micmuted.svg"
theme.icon_bt_on = theme.icon_folder.."bluetooth-active.svg"
theme.icon_bt_off = theme.icon_folder.."bluetooth-disabled.svg"
theme.icon_bt_connected = theme.icon_folder.."bluetooth-connected.svg"


 --Tasklist
theme.tasklist_font = theme.font.." bold 10"
theme.tasklist_disable_icon = true
theme.tasklist_plain_task_name = true
theme.tasklist_bg_focus = x.color0
theme.tasklist_fg_focus = x.foreground
theme.tasklist_bg_normal = "#00000000"
theme.tasklist_fg_normal = x.foreground.."77"
theme.tasklist_bg_minimize = "#00000000"
theme.tasklist_fg_minimize = x.color8
theme.tasklist_font_minimized = "sans italic 8"
theme.tasklist_bg_urgent = x.color1
theme.tasklist_fg_urgent = x.color0
theme.tasklist_spacing = dpi(0)
theme.tasklist_align = "center"

-- Sidebar (Sidebar items can be customized in sidebar.lua)
theme.sidebar_bg = x.background
theme.sidebar_fg = x.color7
theme.sidebar_opacity = 1
theme.sidebar_position = "left" -- left or right
theme.sidebar_width = dpi(300)
theme.sidebar_x = 0
theme.sidebar_y = 0
theme.sidebar_border_radius = dpi(40)

-- Dashboard
theme.dashboard_bg = x.color0.."CC"
theme.dashboard_fg = x.color7

-- Exit screen
theme.exit_screen_bg = "#000000" .. "cc"
theme.exit_screen_fg = x.color7
theme.exit_screen_font = "Fira Sans 20"
theme.exit_screen_icon_size = dpi(300)

-- Lock screen
theme.lock_screen_bg = "#000000".."FF"
theme.lock_screen_fg = "#ffffff"

-- Prompt
theme.prompt_fg = x.foreground

-- Tag names
--theme.tagnames = {"","","","","","","","","",}
theme.tagnames = {"tty","web","chat","office","music","code","games","files","misc"}
--theme.tagnames = {"","","","","","","","",""}
theme.tag_icon_path = "~/.config/awesome/icons/tag-list/"

-- Text Taglist (default)
theme.taglist_font = theme.font .." 7"
theme.taglist_shape_border_color = x.background
theme.taglist_shape_border_color_focus = x.background
theme.taglist_shape_border_width = dpi(0)
theme.taglist_shape = helpers.rrect(4)
theme.taglist_bg_focus = x.background
theme.taglist_fg_focus = x.foreground
theme.taglist_bg_occupied = x.background.. "00"
theme.taglist_fg_occupied = x.foreground.."50"
theme.taglist_bg_empty = x.background .. "00"
theme.taglist_fg_empty = x.foreground .. "30"
theme.taglist_bg_urgent = x.color1
theme.taglist_fg_urgent = x.foreground
theme.taglist_disable_icon = false
theme.taglist_spacing = dpi(2)
-- Generate taglist squares:
local taglist_square_size = dpi(0)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_focus
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

-- Variables set for theming the menu:
theme.menu_height = dpi(35)
theme.menu_width  = dpi(180)
theme.menu_bg_normal = x.color0
theme.menu_fg_normal= x.color7
theme.menu_bg_focus = x.color8 .. "55"
theme.menu_fg_focus= x.color7
theme.menu_border_width = dpi(0)
theme.menu_border_color = x.color0

-- Minimal tasklist widget variables
theme.minimal_tasklist_visible_clients_color = x.color4
theme.minimal_tasklist_visible_clients_text = ""
theme.minimal_tasklist_hidden_clients_color = x.color7
theme.minimal_tasklist_hidden_clients_text = ""

-- Mpd song
theme.mpd_song_title_color = x.color7
theme.mpd_song_artist_color = x.color7
theme.mpd_song_paused_color = x.color8

-- Volume bar
theme.volume_bar_active_color = x.color5
theme.volume_bar_active_background_color = x.color0
theme.volume_bar_muted_color = x.color8
theme.volume_bar_muted_background_color = x.color0

-- Temperature bar
theme.temperature_bar_active_color = x.color1
theme.temperature_bar_background_color = x.color0

-- Battery bar
theme.battery_bar_active_color = x.color6
theme.battery_bar_background_color = x.color0

-- CPU bar
theme.cpu_bar_active_color = x.color2
theme.cpu_bar_background_color = x.color0

-- RAM bar
theme.ram_bar_active_color = x.color4
theme.ram_bar_background_color = x.color0

-- Brightness bar
theme.brightness_bar_active_color = x.color3
theme.brightness_bar_background_color = x.color0

-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(
    theme.menu_height, theme.bg_focus, theme.fg_focus
)

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = "/usr/share/icons/Tela-Circle"

-- Layoutbox icons
theme.layout_max = '~/.config/awesome/icons/layouts/max.svg'
theme.layout_tile = '~/.config/awesome/icons/layouts/tile.svg'
theme.layout_floating = '~/.config/awesome/icons/layouts/floating.svg'

-- Hotkeys Widget
theme.hotkeys_bg = "#00000090"
theme.hotkeys_fg = x.foreground
theme.hotkeys_modifiers_fg = x.color4
theme.hotkeys_label_fg = x.background
theme.hotkeys_label_bg = x.color1
theme.hotkeys_label_font = theme.font .. " 12"
theme.hotkeys_font = theme.font .. " 12"
theme.hotkeys_description_font = theme.font .. " 12"
theme.hotkeys_group_margin = dpi(10)

theme.groups_bg = "#ff0000"
theme.leave_event = "#00ff00"
theme.press_event = "#0000ff"
theme.release_event = "#ffffff"

theme.master_width_factor = 0.6

return theme
