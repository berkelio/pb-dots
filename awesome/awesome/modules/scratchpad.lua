local awful = require("awful")
local bling = require("modules.bling")
local rubato = require("modules.rubato")
local beautiful = require("beautiful")

local screen_width = awful.screen.focused().geometry.width
local screen_height = awful.screen.focused().geometry.height


local term_y = rubato.timed {
  pos = -200,
  rate = 60,
  easing = rubato.quadratic,
  intro = 0.1,
  duration = 0.3,
  awestore_compat = true -- This option must be set to true.
}

local term_x = rubato.timed {
  pos = 0,
  rate = 60,
  easing = rubato.quadratic,
  intro = 0.1,
  duration = 0.3,
  awestore_compat = true -- This option must be set to true.
}

local terminal_scratch = bling.module.scratchpad:new { 
    command = "kitty -c ~/.config/kitty/scratchpad.conf --class term_scratchpad",                   -- How to spawn the scratchpad
    rule = { class = "term_scratchpad" },                        -- The rule that the scratchpad will be searched by
    sticky = true,                                    -- Whether the scratchpad should be sticky
    autoclose = true,                                 -- Whether it should hide itself when losing focus
    floating = true,                                  -- Whether it should be floating
    geometry = {
      width=screen_width-dpi(2),
      height=(screen_height-beautiful.wibar_height)*0.5,
      x=0, 
      y=0,
    }, -- The geometry in a floating state
    border_width = dpi(0),
    reapply = true,                                   -- Whether all those properties should be reapplied on every new opening of the scratchpad (MUST BE TRUE FOR ANIMATIONS)
    dont_focus_before_close  = false,                 -- When set to true, the scratchpad will be closed by the toggle function regardless of whether its focused or not. When set to false, the toggle function will first bring the scratchpad into focus and only close it on a second call
    --rubato = {x = term_x, y = term_y}                -- Optional. This is how you can pass in the stores for animations. If you don't want animations, you can ignore this option.
}

awesome.connect_signal("scratch::term", function() terminal_scratch:toggle() end)


local todo_scratch = bling.module.scratchpad:new { 
  command = 'kitty --class "TODO" -e emacs -nw ~/syncthing/todo.org', -- How to spawn the scratchpad
  rule = { class = "TODO" },                        -- The rule that the scratchpad will be searched by
  sticky = true,                                    -- Whether the scratchpad should be sticky
  autoclose = true,                                 -- Whether it should hide itself when losing focus
  floating = true,                                  -- Whether it should be floating
  geometry = {
    width=screen_width*0.5,
    height=(screen_height-beautiful.wibar_height)*0.5,
    x=(screen_width-((screen_width-(beautiful.border_width * 2))*0.5))/2, 
    y=(screen_height-((screen_height)*0.5))/2,
  }, -- The geometry in a floating state
  border_width = dpi(0),
  reapply = true,                                   -- Whether all those properties should be reapplied on every new opening of the scratchpad (MUST BE TRUE FOR ANIMATIONS)
  dont_focus_before_close  = false,                 -- When set to true, the scratchpad will be closed by the toggle function regardless of whether its focused or not. When set to false, the toggle function will first bring the scratchpad into focus and only close it on a second call
  --rubato = {x = term_x, y = term_y}                -- Optional. This is how you can pass in the stores for animations. If you don't want animations, you can ignore this option.
}

awesome.connect_signal("scratch::todo", function() todo_scratch:toggle() end)

local todo_scratch = bling.module.scratchpad:new { 
  command = 'logseq', -- How to spawn the scratchpad
  rule = { class = "logseq" },                        -- The rule that the scratchpad will be searched by
  sticky = true,                                    -- Whether the scratchpad should be sticky
  autoclose = true,                                 -- Whether it should hide itself when losing focus
  floating = true,                                  -- Whether it should be floating
  geometry = {
    width=screen_width*0.9,
    height=(screen_height-beautiful.wibar_height)*0.9,
    x=(screen_width-((screen_width-(beautiful.border_width * 2))*0.9))/2, 
    y=(screen_height-((screen_height)*0.9))/2,
  }, -- The geometry in a floating state
  border_width = dpi(0),
  reapply = true,                                   -- Whether all those properties should be reapplied on every new opening of the scratchpad (MUST BE TRUE FOR ANIMATIONS)
  dont_focus_before_close  = false,                 -- When set to true, the scratchpad will be closed by the toggle function regardless of whether its focused or not. When set to false, the toggle function will first bring the scratchpad into focus and only close it on a second call
  --rubato = {x = term_x, y = term_y}                -- Optional. This is how you can pass in the stores for animations. If you don't want animations, you can ignore this option.
}

awesome.connect_signal("scratch::obsidian", function() todo_scratch:toggle() end)
