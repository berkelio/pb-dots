-- awesome_mode: api-level=4:screen=on
-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
--package.loaded["naughty.dbus"] = {} 
pcall(require, "luarocks.loader")
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
local wibox = require("wibox")
local ruled = require("ruled")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
local naughty = require("naughty")


-- Enable hotkeys help widget for VIM and other apps when client with a matching name is opened:
--require("awful.hotkeys_popup.keys")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
naughty.connect_signal("request::display_error", function(message, startup)
    naughty.notification {
        urgency = "critical",
        title   = "Oops, an error happened"..(startup and " during startup!" or "!"),
        message = message
    }
end)
-- }}}

--naughty.connect_signal("request::display", function() end)

-- Configuración del tema
screen_width = awful.screen.focused().geometry.width
screen_height = awful.screen.focused().geometry.height

local beautiful = require("beautiful")

local xrdb = beautiful.xresources.get_current_theme()
-- Make dpi function global
    dpi = beautiful.xresources.apply_dpi

-- Make xresources colors global
    x = {
        --           xrdb variable
        background = xrdb.background,
        foreground = xrdb.foreground,
        color0     = xrdb.color0,
        color1     = xrdb.color1,
        color2     = xrdb.color2,
        color3     = xrdb.color3,
        color4     = xrdb.color4,
        color5     = xrdb.color5,
        color6     = xrdb.color6,
        color7     = xrdb.color7,
        color8     = xrdb.color8,
        color9     = xrdb.color9,
        color10    = xrdb.color10,
        color11    = xrdb.color11,
        color12    = xrdb.color12,
        color13    = xrdb.color13,
        color14    = xrdb.color14,
        color15    = xrdb.color15,
    }

    beautiful.init(os.getenv("HOME") .. "/.config/awesome/theme.lua")

-- Load Configuration modules.    
local bling = require("modules.bling")
local scratchpad = require("modules.scratchpad")
require('config')

-- Layout Elements
require('layout.bars.wibar')
require('layout.bars.tray')
--require('layout.sidebar')
require('layout.dash.task')
--require('layout.widgets.clock')
--require('layout.bars.tags')
--require('layout.bars.battery')
--require("layout.exit_screen") -- Exit screen
require('layout.window_switcher')

-- Load titlebars
--local decorations = require("layout.titlebars")
--decorations.init()
local_decorations = require("layout.titlebars.borders")

-- notifications & notifications center
local notifications = require("layout.notifications")
notifications.init(notification_theme)

require('config.rules')
require('config.keys')
require('config.apps')
require('config.signals')
require("evil")

require("layout.popup")

local lock_screen = require("layout.lock_screen")
lock_screen.init()

-- {{{ Mouse bindings
awful.mouse.append_global_mousebindings({
    --awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewprev),
    awful.button({ }, 5, awful.tag.viewnext),
})
-- }}}

-- Destroy notifications
--naughty.connect_signal("request::display", function() end)

-- ### Auto Start Applications
awful.spawn.with_shell(os.getenv("HOME") .. "/.config/awesome/config/autostart.sh")

-- Garbage collection
collectgarbage("setpause", 110)
collectgarbage("setstepmul", 1000)
