-- Monitoring
require("evil.cpu")
require("evil.ram")
--require("evil.temperature")
require("evil.battery")
require("evil.disk")

-- User controlled
require("evil.volume")
require("evil.microphone")
--require("evil.archupdates")
--require("evil.mpd")
require("evil.spotify")
require("evil.brightness")
require("evil.tasks")