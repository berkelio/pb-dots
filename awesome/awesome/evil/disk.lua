-- Provides:
-- evil::disk
--      used (integer - mega bytes)
--      total (integer - mega bytes)
local awful = require("awful")
local helpers = require("helpers")

local update_interval = 360 -- every 6 minutes

-- Use /dev/sdxY according to your setup
local disk_script = [[
    bash -c "
        df /dev/nvme0n1p2 | tail -1 | awk '{print $5}' | cut -d "%" -f1
    "
]]

-- Periodically get disk space info
awful.widget.watch(disk_script, update_interval, function(_, stdout)
    local pct = tonumber(stdout)
    awesome.emit_signal("evil::disk", pct)
end)
