-- Provides:
-- evil::microphone
--      muted (boolean)
local awful = require("awful")
local muted_old = -1

local function emit_microphone_info()
    -- See evil/volume.lua for the reason why we print the +7 and +11 lines after '* index'
    awful.spawn.easy_async_with_shell("amixer -D pulse sget Capture | tail -1 | awk '{print $6}'", function(stdout)
        local muted = string.match(stdout, "%[(o%D%D?)%]")
        local muted = tostring(muted)
        if not (muted == muted_old) then
            awesome.emit_signal("evil::microphone", muted)
            muted_old = muted
        end
    end)
end

-- Run once to initialize widgets
emit_microphone_info()

-- Sleeps until pactl detects an event (microphone volume up / down / (un)mute)
local microphone_script = [[
  bash -c '
  LANG=en pactl subscribe 2> /dev/null | grep --line-buffered "source #"
  ']]

-- Kill old pactl subscribe processes
awful.spawn.easy_async_with_shell("LANG=en ps x | grep \"pactl subscribe\" | grep -v grep | awk '{print $1}' | xargs kill", function ()
    -- Run emit_microphone_info() with each line printed
    awful.spawn.with_line_callback(microphone_script, {
        stdout = function(line)
            emit_microphone_info()
        end
    })
end)
