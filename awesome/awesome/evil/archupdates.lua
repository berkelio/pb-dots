-- Provides:
-- evil::ram
--      used (integer - mega bytes)
--      total (integer - mega bytes)
local awful = require("awful")

local update_interval = 7200
local update_script = [[
  sh -c "
    checkupdates | wc -l
  "]]

-- Periodically get ram info
awful.widget.watch(update_script, update_interval, function(widget, stdout)  
    awesome.emit_signal("evil::arch_updates", tonumber(stdout))
end)
