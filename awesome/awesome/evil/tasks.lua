-- Provides:
-- evil::brightness
--      percentage (integer)
local awful = require("awful")

-- Subscribe to backlight changes
-- Requires inotify-tools
local tasks_subscribe_script = [[
   bash -c "
    while (inotifywait -r -e modify ~/syncthing/todo.org -qq) do echo; done
"]]

local script = [[
    bash -c '
        ~/.config/awesome/config/utils/tasks.sh
  ']]

local emit_tasks_info = function()
    awful.spawn.with_line_callback(script, {
        stdout = function(line)
            pendientes = tonumber(line:match('TO:(.*):DO'))
            completas = tonumber(line:match('DO:(.*):SUM:'))
            total = tonumber(line:match(':SUM:(.*)'))
            awesome.emit_signal("evil::tasks", completas, pendientes, total)
        end
    })
end

-- Run once to initialize widgets
emit_tasks_info()

-- Kill old inotifywait process
awful.spawn.easy_async_with_shell("LANG=en ps x | grep \"inotifywait -r -e modify ~/syncthing/todo.org\" | grep -v grep | awk '{print $1}' | xargs kill", function ()
    -- Update brightness status with each line printed
    awful.spawn.with_line_callback(tasks_subscribe_script, {
        stdout = function(_)
            emit_tasks_info()
        end
    })
end)
