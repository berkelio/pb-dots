-- Provides:
-- evil::spotify
--      artist (string)
--      song (string)
--      status (string) [playing | paused | stopped]
local awful = require("awful")
local helpers = require("helpers")
  
local function emit_info(playerctl_output)
    local artist = playerctl_output:match('artist_start(.*)album_start')
    local album = playerctl_output:match('album_start(.*)title_start')
    local title = playerctl_output:match('title_start(.*)albumart_start')
    local art_url = playerctl_output:match('albumart_start(.*)status_start')
    local status = playerctl_output:match('status_start(.*)'):lower()
    status = string.gsub(status, '^%s*(.-)%s*$', '%1')
    art_url = art_url:gsub('%\n', '')
    art_url = art_url:gsub("open.spotify.com", "i.scdn.co")
       
    awesome.emit_signal("evil::spotify", artist, title, album, status, art_url)
end

-- Sleeps until spotify changes state (pause/play/next/prev)
local spotify_script = [[
  sh -c '
    playerctl metadata -p spotify --format 'artist_start{{artist}}album_start{{album}}title_start{{title}}albumart_start{{mpris:artUrl}}status_start{{status}}' --follow
  ']]


-- playerctl metadata -p spotify --format 'artist_start{{artist}}album_start{{album}}title_start{{title}}status_start{{status}}' --follow

-- Kill old playerctl process
awful.spawn.easy_async_with_shell("ps x | grep \"playerctl metadata\" | grep -v grep | awk '{print $1}' | xargs kill", function ()
    -- Emit song info with each line printed
    awful.spawn.with_line_callback(spotify_script, {
        stdout = function(line)
            emit_info(line)
        end
    })
end)
