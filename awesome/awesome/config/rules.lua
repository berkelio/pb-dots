local ruled = require("ruled")
local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")
local keys = require('config.keys')

-- {{{ Rules
-- Rules to apply to new clients.
ruled.client.connect_signal("request::rules", function()
    -- All clients will match this rule.
    ruled.client.append_rule {
        id         = "global",
        rule       = { },
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            screen = awful.screen.focused,
            shape = function(cr, width, height)
              gears.shape.rounded_rect(cr, width, height, beautiful.client_radius)
            end,
            size_hints_honor = false,
            honor_workarea = true,
            honor_padding = true,
            maximized = false,
            titlebars_enabled = beautiful.titlebars_enabled,
            maximized_horizontal = false,
            maximized_vertical = false,
            placement = awful.placement.centered,
        }
    }

    -- Dialogs
    ruled.client.append_rule {
        id        = 'dialog',
        rule_any  = { 
            type  = { 'dialog', "Dialog"},
            role  = {'Dialog', "dialog"},
            class = { 
                'Wicd-client.py', 
                'calendar.google.com',
            }
        },
        properties = { 
            titlebars_enabled = true,
            floating = true,
            above = true,
            draw_backdrop = true,
            skip_decoration = false,
            shape = function(cr, width, height)
                        gears.shape.rounded_rect(cr, width, height, beautiful.client_radius)
                    end,
            placement = awful.placement.centered,
            callback = function (c)
                awful.placement.centered(c,{honor_padding = true, honor_workarea=true})
            end
        }
    }

    -- Floating clients.
    ruled.client.append_rule {
        id       = "floating",
        rule_any = {
            instance = { "copyq", "pinentry" },
            class    = {
                "Sxiv",
                "Pavucontrol",
                "Blueberry.py",
                "Xfce4-power-manager-settings",
                "Xfce4-appearance-settings",
                "MultiMC",
                "System-config-printer.py",
                "Gnome-calculator",
                "Blueman-manager"
            },
            -- Note that the name property shown in xprop might be set slightly after creation of the client
            -- and the name shown there might not match defined rules here.
            name    = {
                "Event Tester",  -- xev.
            },
            role    = {
                "AlarmWindow",    -- Thunderbird's calendar.
                "ConfigManager",  -- Thunderbird's about:config.
                "pop-up",         -- e.g. Google Chrome's (detached) Developer Tools.
            }
        },
        properties = { floating = true }
    }

    -- Add titlebars to normal clients and dialogs
    ruled.client.append_rule {
        id         = "titlebars",
        rule_any   = { type = { "normal", "dialog" } },
        properties = { titlebars_enabled = true      }
    }

    -- Remove titlebars off
    ruled.client.append_rule {
        id         = "titlebars_off",
        rule_any   = { 
            class = { 
                "Blueberry.py",
                "Brave-browser",
                "Xfce4-power-manager-settings",
                "Steam",
                "Xfce4-appearance-settings",
                "obsidian",
                "Lutris",
                "Gnome-control-center",
                "Gnome-calendar",
            },
            name = {
                "Fallout 4",
            }
        },
        properties = { titlebars_enabled = false }
    }

    -- Set Firefox to always map on the tag named "2" on screen 1.
    ruled.client.append_rule {
        rule_any = {
            class = {
                "Firefox",
                "firefox",
                "Brave-browser"
            },
        },
        except_any = {
            role = { "GtkFileChooserDialog" },
            instance = { "Toolkit" },
            type = { "dialog" }
        },
        properties = { screen = 1, tag = awful.screen.focused().tags[2] },
    }

    -- Set Nautilus in desktop 8
    ruled.client.append_rule {
        rule_any = {
            class = {
                "Nautilus",
                "Thunar"
            },
        },
        except_any = {
            role = { "GtkFileChooserDialog" },
            instance = { "Toolkit" },
            type = { "dialog" }
        },
        properties = { screen = 1, tag = awful.screen.focused().tags[8] },
    }

    ruled.client.append_rule {
        rule_any = {
            class = {
                "Code"
            },
        },
        except_any = {
            role = { "GtkFileChooserDialog" },
            instance = { "Toolkit" },
            type = { "dialog" }
        },
        properties = { screen = 1, tag = awful.screen.focused().tags[6] },
    }

    ruled.client.append_rule {
        rule_any = {
            class = {
                "Spotify"
            },
        },
        except_any = {
            role = { "GtkFileChooserDialog" },
            instance = { "Toolkit" },
            type = { "dialog" }
        },
        properties = { screen = 1, tag = awful.screen.focused().tags[5]},
    }

    ruled.client.append_rule {
        rule_any = {
            class = {
                "Chromium",
                "Slack",
            },
        },
        except_any = {
            role = { "GtkFileChooserDialog" },
            instance = { "Toolkit" },
            type = { "dialog" }
        },
        properties = { screen = 1, tag = awful.screen.focused().tags[3]},
    }

    --GNOME-CALENDAR
    ruled.client.append_rule {
        rule_any = {
            class = {
               "Gnome-calendar"
            },
        },
        properties = { floating = true},
    }

    --GNOME-CALENDAR
    ruled.client.append_rule {
        rule_any = {
            class = {
                "term_scratchpad"
            },
        },
        properties = { 
            floating = true,
            titlebars_enabled = false,
            x = 0,
            y = 0,
            border_width = dpi(0),
        },
    }
    

    --TODO
    ruled.client.append_rule {
        rule_any = {
            class = {
                "TODO"
            },
        },
        properties = { floating = true},
    }


    -- #floating clients
    ruled.client.append_rule {
        rule_any = {
            class = {
               
            },
        },
        properties = { floating = true},
    }

    ruled.client.append_rule {
        rule_any = { class = { "bashtop", }, },
        properties = { floating = true, width=screen_width*0.9, height=screen_height*0.9, placement = awful.placement.centered  },
        callback = function (c)
            awful.placement.centered(c,{honor_padding = true, honor_workarea=true})
        end
    }

    --KEEPASS
    ruled.client.append_rule {
        rule_any = { class = { "KeePassXC", }, },
        properties = { floating = true, width=screen_width*0.5, height=screen_height*0.5, placement = awful.placement.centered  },
        callback = function (c)
            awful.placement.centered(c,{honor_padding = true, honor_workarea=true})
        end
    }

    --OBSIDIAN
    ruled.client.append_rule {
        rule_any = { 
            class = { 
                "logseq",
            }, 
        },  
        properties = { floating = true},
    }

    --STEAM
    ruled.client.append_rule {
        rule_any = { class = { "Steam", }, },
        properties = { floating = true, placement = awful.placement.centered, tag = awful.screen.focused().tags[7]},
        callback = function (c)
            awful.placement.centered(c,{honor_padding = true, honor_workarea=true})
        end
    }

    --FALLOUT 4
    ruled.client.append_rule {
        rule_any = { name = { "Fallout 4", }, },
        properties = { floating = false, placement = awful.placement.centered, tag = awful.screen.focused().tags[7]},
        callback = function (c)
            awful.placement.centered(c,{honor_padding = true, honor_workarea=true})
        end
    }


    --REMMINA
    ruled.client.append_rule {
        rule_any = { name = { 
                "Cliente de escritorio remoto Remmina", 
                "Remmina Remote Desktop Client",
            },  
        },
        properties = { floating = true, placement = awful.placement.centered, width=screen_width*0.5, height=screen_height*0.5, tag = awful.screen.focused().tags[4], titlebars_enabled = false},
        callback = function (c)
            awful.placement.centered(c,{honor_padding = true, honor_workarea=true})
        end
    }

    --SelectWallpaper
    ruled.client.append_rule {
        rule_any = { class = { "Sxiv", }, },
        properties = { floating = true, width=screen_width*0.75, height=screen_height*0.75, placement = awful.placement.centered  },
        callback = function (c)
            awful.placement.centered(c,{honor_padding = true, honor_workarea=true})
        end
    }
   
    
end)

-- }}}
