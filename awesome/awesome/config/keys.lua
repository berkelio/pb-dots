local awful = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup")
local apps = require("config.apps")
local helpers = require("helpers")
local naughty = require("naughty")

lock_screen = require("layout.lock_screen")

local hotkeys_popup = require('awful.hotkeys_popup').widget
local hotkeys = require("awful.hotkeys_popup")
local my_hotkeys_popup = hotkeys_popup.new({width = screen_width*0.98, height = screen_height*0.95, bg = x.background, border_width=dpi(0) });

-- #superkey
superkey = "Mod4"
altkey = "Mod1"
ctrlkey = "Control"
shiftkey = "Shift"

awful.keyboard.append_global_keybindings({
    awful.key({ superkey }, 'F1', function() my_hotkeys_popup:show_help() end, {description = 'Mostrar esta ayuda', group = 'awesome'}),
    awful.key({ ctrlkey, altkey }, 'w', function() awful.spawn.with_shell("sxiv -N SelectWallpaper -t ~/Imágenes/wallpapers") end, {description = 'Mostrar esta ayuda', group = 'awesome'}),
    awful.key({ superkey, "Control" }, "r", awesome.restart, {description = "reload awesome", group = "awesome"}),

    -- Screenshots
    awful.key( { }, "Print", function() awful.spawn.with_shell("~/.scripts/screeny screenshot") end, {description = "Guardar una captura de pantalla", group = "screenshots"}),
    awful.key( { ctrlkey  }, "Print", function() awful.spawn.with_shell("~/.scripts/screeny window") end, {description = "Guardar un area de la pantalla", group = "screenshots"}),
    awful.key( { shiftkey }, "Print", function() awful.spawn.with_shell("~/.scripts/screeny region") end, {description = "Copiar un area de la pantalla al portapapeles", group = "screenshots"}),
    awful.key( { superkey }, "Print", function() awful.spawn.with_shell("~/.scripts/screeny browse") end, {description = "Explorar capturas de pantalla.", group = "screenshots"}),
    awful.key( { superkey, shiftkey }, "Print", function() awful.spawn.with_shell("~/.scripts/screeny view") end, {description = "Abrir la captura más reciente con Gimp", group = "screenshots"}),
    awful.key( { ctrlkey, shiftkey }, "Print", function() awful.spawn.with_shell("~/.scripts/screeny edit") end, {description = "Abrir la captura más reciente con Gimp", group = "screenshots"}),

    -- #ROFI
    awful.key({ superkey }, "a", function() awful.spawn.with_shell("~/.scripts/rofi-run launcher") end, {description = "lanzador de aplicaciones", group = "Lanzadores"}),
    awful.key({ superkey }, "r", function() awful.spawn.with_shell("~/.scripts/rofi-run run") end, {description = "Ejecutar comando", group = "Lanzadores"}),
    awful.key({ superkey }, "f", function() awful.spawn.with_shell("~/.scripts/rofi-edit") end, {description = "Buscar archivos de configuracion", group = "Lanzadores"}),
    --awful.key({ superkey,}, "n", function() awful.spawn.with_shell("~/.scripts/rofi-notes") end, {description = "Buscar notas", group = "Lanzadores"}),
    awful.key({ ctrlkey, altkey}, "d", function() awful.spawn.with_shell("~/.scripts/rofi-display") end, {description = "Abrir configuración de pantallas", group = "Lanzadores"}),
    awful.key({ ctrlkey, altkey}, "s", function() awful.spawn.with_shell("~/.scripts/rofi-ssh") end, {description = "Conexiones SSH", group = "Lanzadores"}),
    awful.key({ superkey }, "p", function() awful.spawn.with_shell("~/.scripts/rofi-pass") end, {description = "Buscar contraseñas en keepassxc", group = "Lanzadores"}),
    awful.key({ superkey }, "g", function() awful.spawn.with_shell("~/.scripts/rofi-run games") end, {description = "Lanzador de Juegos", group = "Lanzadores"}),
    awful.key({ superkey }, "t", function() awful.spawn.with_shell("~/.scripts/rofi-addtask") end, {description = "(Des)Conectar VPN", group = "Lanzadores"}),
    --awful.key({ superkey }, "s", function() awesome.emit_signal("toggle_status") end, {description = "Mostrar Estado", group = "Lanzadores"}),
    --awful.key({ superkey }, "d", function() awesome.emit_signal("toggle_wibar") end, {description = "Mostrar Estado", group = "Lanzadores"}),
    --awful.key({ superkey }, "d", function() awesome.emit_signal("toggle_wibar") end, {description = "Mostrar Estado", group = "Lanzadores"}),
    --awful.key({ superkey }, "s", function() awesome.emit_signal("toggle_sidebar") end, {description = "Mostrar Estado", group = "Lanzadores"}),
    ---awful.key({ superkey }, "s", function() awful.spawn.with_shell("~/.scripts/status") end, {description = "Mostrar la hora", group = "Lanzadores"}),

    --awful.key({ superkey }, "s", function() cc_toggle(awful.screen.focused()) end, {description = "Mostrar Estado", group = "Lanzadores"}),
    --awful.key({ ctrlkey, altkey }, "t", function() dash_toggle(awful.screen.focused()) end, {description = "(Des)Conectar VPN", group = "Lanzadores"}),


    awful.key({ ctrlkey, altkey }, "Escape", function() awful.spawn.with_shell("~/.scripts/rofi-powermenu") end, {description = "Administrador de conexiones RDP", group = "Lanzadores"}),
    --awful.key({ ctrlkey, altkey }, "Escape", function() exit_screen_show() end, {description = "Administrador de conexiones RDP", group = "Lanzadores"}),
    awful.key({ superkey }, "v", function() awful.spawn.with_shell("~/.scripts/rofi-vpn") end, {description = "(Des)Conectar VPN", group = "Lanzadores"}),
    awful.key({ superkey }, "d", function() awesome.emit_signal("toggle_tagbox") end, {description = "(Des)Conectar VPN", group = "Lanzadores"}),
    awful.key({}, "XF86PowerOff", function() awful.spawn.with_shell("~/.scripts/rofi-powermenu") end, {description = "Administrador de conexiones RDP", group = "Lanzadores"}),

    -- #APPS
    awful.key({ superkey }, "q", apps.browser, {description = "Explorador web", group = "Lanzadores"}),
    awful.key({ superkey }, "w", function () awful.spawn.raise_or_spawn('thunar') end, {description = "Explorador de archivos", group = "Lanzadores"}),
    awful.key({ superkey,}, "Return", function () awful.spawn(terminal) end, {description = "open a terminal", group = "Lanzadores"}), 
    awful.key({ superkey }, "l", function () awful.spawn.with_shell("betterlockscreen -l dim") end, {description = "Bloquear la pantalla", group = "awesome"}), 
    awful.key({ ctrlkey, altkey }, "p", apps.process_monitor, {description = "Monitor de procesos", group = "Lanzadores"}),
    awful.key({ ctrlkey, altkey}, "b", function() awful.spawn.raise_or_spawn("blueman-manager") end, {description = "bluetooth manager", group = "Lanzadores"}),
    --awful.key({ ctrlkey, altkey }, "b", function() awful.spawn.with_shell("~/.scripts/rofi-bt") end, {description = "bluetooth manager", group = "Lanzadores"}),
    --awful.key({ superkey }, "o", function () awful.spawn.with_shell("~/.scripts/runorraise obsidian") end, {description = "Obsidian", group = "Lanzadores"}),
    awful.key({ superkey }, "m", apps.music, {description = "Spotify", group = "Lanzadores"}),
    
    -- #Scratchpad
    --awful.key({ superkey }, "m", function () awesome.emit_signal("scratch::music") end, {description = "Spotify-Tui", group = "Lanzadores"}),
    awful.key({ superkey }, "Escape", function () awesome.emit_signal("scratch::term") end, {description = "Abrir Terminal Flotante", group = "Lanzadores"}),
    awful.key({ ctrlkey, altkey }, "t", function () awesome.emit_signal("scratch::todo") end, {description = "Todoist", group = "Scratchpad"}),
    awful.key({ ctrlkey, altkey }, "o", function () awesome.emit_signal("scratch::obsidian") end, {description = "Emacs - Todo.org", group = "Scratchpad"}),
    awful.key({ ctrlkey, altkey }, "c", function () awesome.emit_signal("scratch::calendar") end, {description = "Calendario", group = "Scratchpad"}),
    
    -- #Volumen
    awful.key( { superkey }, "-",function() helpers.volume_control("down") end, {description = "lower volume", group = "Sonido"}),
    awful.key( { superkey }, "=", function() helpers.volume_control("up") end, {description = "raise volume", group = "Sonido"}),
    awful.key( {ctrlkey, altkey },  "m", function() helpers.volume_control("toggle") end, {description = "(un)mute volume", group = "Sonido"}),
    awful.key( { ctrlkey, altkey }, "v", function() awful.spawn.with_shell("amixer -D pulse sset Capture toggle") end, {description = "(un)mute microphone", group = "Sonido"}),     -- Microphone (V for voice
    awful.key( { },  "XF86AudioMute", function() helpers.volume_control("toggle") end, {description = "(un)mute volume", group = "Sonido"}),
    awful.key( { }, "XF86AudioLowerVolume",function() helpers.volume_control("down") end, {description = "lower volume", group = "Sonido"}),
    awful.key( { }, "XF86AudioRaiseVolume", function() helpers.volume_control("up") end, {description = "raise volume", group = "Sonido"}),

    -- #Media Control
    -- Media keys
    awful.key({ superkey }, "/", function () awful.spawn('playerctl -p ncspot,spotify next', false) end, {description = "Reproducir canción siguiente", group = "Music Player"}),
    awful.key({ superkey }, ",", function () awful.spawn('playerctl -p ncspot,spotify previous', false) end, {description = "Reproducir canción anterior", group = "Music Player"}),
    awful.key({ superkey }, ".", function () awful.spawn('playerctl -p ncspot,spotify play-pause', false) end, {description = "Reproducir/Pausar Spotify", group = "Music Player"}),

    -- #Brillo
    awful.key( { }, "XF86MonBrightnessDown", function() awful.spawn.with_shell("brightnessctl set 10-") end, {description = "bajar el brillo", group = "Brillo"}),
    awful.key( { }, "XF86MonBrightnessUp", function() awful.spawn.with_shell("brightnessctl set 10+") end, {description = "subir el brillo", group = "Brillo"}),
    awful.key( { superkey, ctrlkey }, "=", function() awful.spawn.with_shell("brightnessctl set 10+") end, {description = "subir el brillo", group = "Brillo"}),
    awful.key( { superkey, ctrlkey }, "-", function() awful.spawn.with_shell("brightnessctl set 10-") end, {description = "bajar el brillo", group = "Brillo"}),
    awful.key( { ctrlkey, altkey }, "n", function() awful.spawn.with_shell("~/.scripts/nightmode") end, {description = "Des/Activar Modo Nocturno", group = "Brillo"}),

    -- #SIGNALS
    awful.key({ ctrlkey, altkey }, "-", function () awesome.emit_signal("toggle_tray") end, {description = "Bloquear visibilidad del area de notificaciones", group = "awesome"}),
    --awful.key({ ctrlkey },"space", function() awful.spawn.with_shell("dunstctl close-all") end, {description = "Eleminar Notificaciones", group = "Notificaciones"}),    -- #Destroy Notification
    awful.key( { ctrlkey }, "space",
    function()
        awesome.emit_signal("elemental::dismiss")
        naughty.destroy_all_notifications()
    end,
    {description = "dismiss notification", group = "notifications"}),

    -- Window switcher
    --awful.key({ altkey }, "Tab", function () awful.spawn('rofi -show window', false) end, {description = "activate window switcher", group = "client"}),
    awful.key({ altkey }, "Tab", function () window_switcher_show(awful.screen.focused()) end, {description = "activate window switcher", group = "client"}),
    
})

-- Tags related keybindings
awful.keyboard.append_global_keybindings({
    awful.key({ superkey,}, "Left",   awful.tag.viewprev, {description = "view previous", group = "tag"}),
    awful.key({ superkey,}, "Right",  awful.tag.viewnext, {description = "view next", group = "tag"}),
    --awful.key({ superkey,}, "Escape", awful.tag.history.restore, {description = "go back", group = "tag"}),
})

-- Focus related keybindings
awful.keyboard.append_global_keybindings({

    awful.key({ superkey,}, "j", function() helpers.view_prev_tag_with_client() end, {description = "Ver tag ocupado previo", group = "tag"}),
    awful.key({ superkey,}, "k", function() helpers.view_next_tag_with_client() end, {description = "Ver tag ocupado siguiente", group = "tag"}),
    awful.key({ ctrlkey, altkey}, "j", function () awful.client.focus.byidx(-1) end, {description = "focus next by index", group = "client"}),
    awful.key({ ctrlkey, altkey}, "k", function () awful.client.focus.byidx( 1) end, {description = "focus previous by index", group = "client"}),
    awful.key({ superkey,}, "Tab", function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),

    -- Screen focus
    awful.key({ superkey,}, "Tab", function () awful.screen.focus_relative( 1) end,{description = "focus the next screen", group = "screen"}),
    awful.key({ ctrlkey, superkey}, "Tab",
        function ()
            local c = client.focus
            if c then c:move_to_screen() end
        end, {description = "Move focused window on next screen", group = "Screens management"})
})

-- Layout related keybindings
awful.keyboard.append_global_keybindings({
    awful.key({ superkey, "Shift"}, "j", function () awful.client.swap.byidx(  1) end, {description = "swap with next client by index", group = "client"}),
    awful.key({ superkey, "Shift"}, "k", function () awful.client.swap.byidx( -1)    end, {description = "swap with previous client by index", group = "client"}),
    awful.key({ superkey,}, "u", awful.client.urgent.jumpto, {description = "jump to urgent client", group = "client"}),
    awful.key({ superkey, ctrlkey}, "k", function () awful.tag.incmwfact( 0.05) end, {description = "increase master width factor", group = "layout"}),
    awful.key({ superkey,ctrlkey}, "j",     function () awful.tag.incmwfact(-0.05) end, {description = "decrease master width factor", group = "layout"}),
    awful.key({ superkey,}, "space", function () awful.layout.inc( 1) end, {description = "select next", group = "layout"}),
    awful.key({ superkey, "Shift"   }, "space", function () awful.layout.inc(-1) end, {description = "select previous", group = "layout"}),
})


awful.keyboard.append_global_keybindings({
    
    awful.key {
        modifiers   = { superkey },
        keygroup    = "numrow",
        description = "only view tag",
        group       = "tag",
        on_press    = function (index)
            local screen = awful.screen.focused()
            local tag = screen.tags[index]
            if tag then
                tag:view_only()
            end
        end,
    },

    awful.key {
        modifiers   = { superkey, "Shift" },
        keygroup    = "numrow",
        description = "toggle tag",
        group       = "tag",
        on_press    = function (index)
            local screen = awful.screen.focused()
            local tag = screen.tags[index]
            if tag then
                awful.tag.viewtoggle(tag)
            end
        end,
    },

    awful.key {
        modifiers = { superkey, ctrlkey },
        keygroup    = "numrow",
        description = "move focused client to tag",
        group       = "tag",
        on_press    = function (index)
            if client.focus then
                local tag = client.focus.screen.tags[index]
                if tag then
                    client.focus:move_to_tag(tag)
                end
            end
        end,
    },
     
})

client.connect_signal("request::default_mousebindings", function()
    awful.mouse.append_client_mousebindings({
        awful.button({ }, 1, function (c) c:activate { context = "mouse_click" } end),
        awful.button({ superkey }, 1, function (c) c:activate { context = "mouse_click", action = "mouse_move"  } end),
        awful.button({ superkey }, 3, function (c) c:activate { context = "mouse_click", action = "mouse_resize"} end),
    })
end)

client.connect_signal("request::default_keybindings", function()
    awful.keyboard.append_client_keybindings({
        awful.key({ altkey,ctrlkey }, "f", function (c) c.fullscreen = not c.fullscreen c:raise() end, {description = "toggle fullscreen", group = "client"}),
        awful.key({ ctrlkey, altkey }, "q", function (c) c:kill() end, {description = "close", group = "client"}),
                
        awful.key({ superkey }, "c", function (c)
            awful.placement.centered(c, {honor_workarea = true, honor_padding = true, })
            helpers.single_double_tap(
                nil,
                function ()
                    helpers.float_and_resize(c, screen_width * 0.9, screen_height * 0.9)
                end)
        end),

        awful.key({ superkey}, "c",
        function(c)
            c.fullscreen = false
            c.maximized = false
            c.floating = not c.floating
            c:raise()
            if c.floating then
                c.width = screen_width * 0.75
                c.height = screen_height * 0.75
                placement = awful.placement.centered(c)
            end
        end,
        {description = "toggle floating", group = "client"}),     
    })
end)

-- }}}
