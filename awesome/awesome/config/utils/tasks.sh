#!/bin/env bash

file="~/syncthing/todo.org"
todo=$(cat ~/syncthing/todo.org | grep "* TODO" | wc -l)
done=$(cat ~/syncthing/todo.org | grep "* DONE" | wc -l)
total=$(cat ~/syncthing/todo.org | grep -v "* DONE|* TODO" | wc -l)

echo "TO:$todo:DO:$done:SUM:$total"