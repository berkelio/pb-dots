#!/usr/bin/env bash

## Get info
#IFACE="$(nmcli | grep -i interface | awk '/interface/ {print $2}')"
status="$(nmcli connection show --active | grep -i '-' >> /dev/null && echo active)"

wifi="$(nmcli connection show --active | grep -i wifi >> /dev/null && echo active)"
vpn="$(nmcli connection show --active | grep -i vpn >> /dev/null && echo active)"
cable="$(nmcli connection show --active | grep -i cable >> /dev/null && echo active)"

#name="$(nmcli connection show --active | tail -n1 | awk '{print $1}')"

if [[ $status == "active" ]]; then
    if [[ $vpn == "active" ]]; then
        name="$(nmcli connection show --active | grep -i vpn | tail -n1 | awk '{print $1}')"
        echo "vpn - $name"
    elif [[ $cable == "active" ]]; then
        name="$(nmcli connection show --active | grep -i cable | tail -n1 | awk '{print $1}')"
        echo "cable - $name"
    elif [[ $wifi == "active" ]]; then
        name="$(nmcli connection show --active | grep -i wifi | tail -n1 | awk '{print $1}')"
        echo "wifi - $name"
    fi
else
    echo disconnected
fi