#!/bin/env bash

#STATE=$(cat /sys/class/power_supply/AC/online)
state=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -i state: | awk '{print $2}')

if [[ $state = "fully-charged" ]]; then
    echo "1"
elif [[ $state = "discharging" ]]; then
    echo "discharging"
else 
    echo "3"
fi

#if [[ $1 == pct ]]; then
#    STATE=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -i fully-charged > /dev/null && echo full || echo disch)
#    if [[ $STATE == "full" ]]; then
#        echo "100"
#    else 
#        PCT=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep percentage | awk '{print $2}' | cut -d % -f 1)
#        echo $PCT
#    fi
#fi