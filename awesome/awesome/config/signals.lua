local awful = require("awful")
local beautiful = require("beautiful")

-- Signals
client.connect_signal("manage", function (c)
    if not awesome.startup then awful.client.setslave(c) end
  end)

if beautiful.border_width > 0 then
    client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
    client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
end

-- Set mouse resize mode (live or after)
awful.mouse.resize.set_mode("live")

-- Restore geometry for floating clients (for example after swapping from tiling mode to floating mode)
tag.connect_signal('property::layout', function(t)
    for k, c in ipairs(t:clients()) do
        if awful.layout.get(mouse.screen) == awful.layout.suit.floating then
            local cgeo = awful.client.property.get(c, 'floating_geometry')
            if cgeo then
                c:geometry(awful.client.property.get(c, 'floating_geometry'))
            end
        end
    end
  end)

client.connect_signal('manage', function(c)
    if awful.layout.get(mouse.screen) == awful.layout.suit.floating then
        awful.client.property.set(c, 'floating_geometry', c:geometry())
    end
end)

client.connect_signal('property::geometry', function(c)
    if awful.layout.get(mouse.screen) == awful.layout.suit.floating then
        awful.client.property.set(c, 'floating_geometry', c:geometry())
    end
end)

client.connect_signal("property::minimized", function(c)
    c.minimized = false
end)

-- ==============================================================

-- When switching to a tag with urgent clients, raise them.
-- This fixes the issue (visual mismatch) where after switching to a tag which includes an urgent client, the urgent client is unfocused but still covers all other windows (even the currently focused window).
awful.tag.attached_connect_signal(s, "property::selected", function ()
    local urgent_clients = function (c)
        return awful.rules.match(c, { urgent = true })
    end
    for c in awful.client.iterate(urgent_clients) do
        if c.first_tag == mouse.screen.selected_tag then
            client.focus = c
        end
    end
  end)
  
  -- Raise focused clients automatically
  client.connect_signal("focus", function(c) c:raise() end)
  
  -- Disable ontop when the client is not floating, and restore ontop if needed
  -- when the client is floating again
  -- I never want a non floating client to be ontop.
  client.connect_signal('property::floating', function(c)
    if c.floating then
        if c.restore_ontop then
            c.ontop = c.restore_ontop
        end
    else
        c.restore_ontop = c.ontop
        c.ontop = false
    end
  end)

  client.connect_signal("manage", function (c)
    -- Some applications (like Spotify) does not respect ICCCM rules correctly
    -- and redefine the window class property.
    -- This leads to having window which does *NOT* follow the user rules
    -- defined in the table `awful.rules.rules`.
    c:connect_signal("property::class", awful.rules.apply)

    awful.rules.apply(c)
end)

client.connect_signal("unmanage", function (c)
    c:disconnect_signal("property::class", awful.rules.apply)
end)