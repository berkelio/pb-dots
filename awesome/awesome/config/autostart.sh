#!/usr/bin/env bash
# ---
# Use "run program" to run it only if it is not already running
# Use "program &" to run it regardless
# ---
function run {
    if ! pgrep $1 > /dev/null ;
    then
        $@&
    fi
}

# Load terminal colorscheme and settings
#~/.fehbg
xrdb -merge ~/.Xresources
#setxkbmap latam
setxkbmap us -variant altgr-intl 
xset -b 
emacs --daemon &
run nm-applet
run picom --config ~/.config/picom/picom.conf --experimental-backends &
run xfsettingsd
run xfce4-power-manager
run dropbox start