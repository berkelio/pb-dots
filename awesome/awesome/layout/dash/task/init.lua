local awful = require("awful")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local helpers = require("helpers")
local wibox = require("wibox")
local gears = require("gears")
local rubato = require("modules.rubato")

local tasks = require("layout.dash.task.task")

awful.screen.connect_for_each_screen(function(s)

	dash = wibox({
		width = dpi(1000),
		height = dpi(800),
		bg = x.background,
		border_width = dpi(1),
		border_color = "#000000",
		ontop = true,
		visible = false,
		type = "splash",
		shape = helpers.rrect(12),
	})
		--~~~~~~~~~~~~~~~

	dash:setup {
		{
			tasks,
			expand = "inside",
			layout = wibox.layout.fixed.vertical
		},
		valign = "top",
		layout = wibox.container.place
	}


		-- toggler script
		--~~~~~~~~~~~~~~~
		local screen_backup = 1

		dash_toggle = function(screen)
			dash.visible = not dash.visible

			awful.placement.centered(
			dash, 
			{
				margins = beautiful.useless_gap*4, 
				parent = awful.screen.focused()
			}
		)

		end

end)