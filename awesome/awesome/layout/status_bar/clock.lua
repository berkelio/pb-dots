local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local apps = require('config.apps')
local helpers = require('helpers')

local icon_font = "Material Icons 12"

local clock = {
	{
		{
			font = "Fira Sans 8",
			color = x.color1,
			align = "center",
			valign = "center",
			widget = wibox.widget.textclock(helpers.colorize_text("%d de %B", x.foreground.."dd"))
		},
		{
			font = "Fira Sans Bold 20",
			color = x.color1,
			align = "center",
			valign = "center",
			widget = wibox.widget.textclock(helpers.colorize_text("%H:%M", x.foreground.."dd"))
		},
		nil,
		layout  = wibox.layout.fixed.vertical,
	},
	margins = dpi(5),
	widget = wibox.container.margin
}

return clock