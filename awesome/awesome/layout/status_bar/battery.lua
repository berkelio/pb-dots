local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local apps = require('config.apps')
local helpers = require('helpers')

local batteryicon = wibox.widget {
	font = beautiful.widget_icon_font.." 12",
	valign = "center",
	markup = helpers.colorize_text("",x.foreground),
	widget = wibox.widget.textbox
}

local batteryslider = wibox.widget {
	bar_shape = gears.shape.rounded_rect,
	bar_height = dpi(5),
	bar_color = x.color8,
	bar_active_color = x.color2,
	handle_color = x.color2.."00",
	handle_shape = gears.shape.circle,
	handle_width = dpi(0),
	handle_border_width = dpi(1),
	handle_border_color = x.background,
	maximum = 100,
	widget = wibox.widget.slider
}

local battery = wibox.widget {
	{
		batteryicon,
		right = dpi(15),
		widget = wibox.container.margin
	},
	nil,
	batteryslider,
	forced_height = dpi(20),
	layout = wibox.layout.align.horizontal
}

awesome.connect_signal("evil::battery", function(value)
    batteryslider.value = value
    if value > 20 then
        batteryicon.markup = helpers.colorize_text("",x.foreground)
    elseif value > 10 then
        battery.markup = helpers.colorize_text("",x.foreground)
    else
        battery.markup = helpers.colorize_text("",x.foreground)
    end
end)

return battery