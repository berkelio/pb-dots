local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local apps = require('config.apps')
local helpers = require('helpers')

local icon_font = "Material Icons 12"

local launcherDisplay = wibox {
	width = dpi(200),
	height = dpi(335),
	bg = "#00000090",
	ontop = true,
	visible = false,
	class = "splash",
	shape = helpers.rrect(12)
}

local textbutton = function(args)
	local text = args.text
	local size = args.size or dpi(10)
	local color = args.color or x.color4
	local onclick = args.onclick or function() end
	
	local result = wibox.widget {
		{
			text = text,
			font = "Material Icons " .. size,
			forced_height = dpi(45),
			forced_width = dpi(45),
			align = "center",
			valign = "center",
			markup = helpers.colorize_text(text, x.background),
			widget = wibox.widget.textbox
		},
		bg = color,
		shape = helpers.rrect(22),
		widget = wibox.container.background
	}

	result:connect_signal("button::press", function() 
			awesome.emit_signal("widget::launcher")
			awful.spawn.with_shell(onclick) 
	end)

	return result
end

local create_button = function (symbol, color, bg_color, hover_color)
    local widget = wibox.widget {
        font = "Material Icons 12",
		forced_height = dpi(45),
		forced_width = dpi(45),
        align = "center",
        valign = "center",
        id = "text_role",
        markup = helpers.colorize_text(symbol, color),
        widget = wibox.widget.textbox(),
    }

    local section = wibox.widget {
        widget,
		shape = helpers.rrect(12),
        bg = bg_color,
        widget = wibox.container.background
        }
        

    -- Hover animation
    section:connect_signal("mouse::enter", function ()
        section.bg = hover_color
    end)
    section:connect_signal("mouse::leave", function ()
        section.bg = bg_color
    end)

   helpers.add_hover_cursor(section, "hand1")

    return section
end

local wireless = create_button("", x.color4, "#222", beautiful.hover_color)
local bt = create_button("", x.color4, "#222", beautiful.hover_color)
local mic = create_button("", x.color4, "#222", beautiful.hover_color)


launcherDisplay:setup {
	{
		{
			require('layout.status_bar.clock'),
			top = dpi(15),
			widget = wibox.container.margin,
		},
		{
			{
				require('layout.status_bar.volume'),
				left = dpi(20),
				right = dpi(23),
				bottom = dpi(5),
				widget = wibox.container.margin
			},
			{
				require('layout.status_bar.brightness'),
				left = dpi(20),
				right = dpi(23),
				bottom = dpi(5),
				widget = wibox.container.margin
			},
			{
				require('layout.status_bar.fs'),
				left = dpi(20),
				right = dpi(23),
				bottom = dpi(5),
				widget = wibox.container.margin
			},
			layout = wibox.layout.fixed.vertical
		},
		{
			{
				{
					require('layout.status_bar.button-net'),
					require('layout.status_bar.button-bt'),
					mic,
					expand = "none",
					layout = wibox.layout.align.horizontal
				},
				nil,
				{
					textbutton{ text="", size="10", onclick=apps.browser },
					textbutton{ text="", size="10", onclick=apps.terminal },
					textbutton{ text="", size="10", onclick=apps.files },
					expand = "none",
					layout = wibox.layout.align.horizontal
				},
				spacing = dpi(10),
				layout = wibox.layout.fixed.vertical,
			},
			left = dpi(15),
			right = dpi(15),
			widget = wibox.container.margin
		},
		layout = wibox.layout.align.vertical
	},
	valign = "top",
	layout = wibox.container.place
}

awesome.connect_signal("toggle_status", function()
	launcherDisplay.visible = not launcherDisplay.visible
	
	awful.placement.bottom_right(
		launcherDisplay, 
		{
			margins = { 
				bottom = dpi(30), 
				right = dpi(10)
			}, 
			parent = awful.screen.focused()
		}
	)
end)
