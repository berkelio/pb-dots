local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local apps = require('config.apps')
local helpers = require('helpers')

local icono = ""
local bg_color = "#22222200"
local hover_color = beautiful.hover_color

local icon = wibox.widget {
	font = beautiful.widget_icon_font .. " 12",
	align = "center",
	valign = "center",
	id = "text_role",
	markup = helpers.colorize_text(icono, x.foreground),
	widget = wibox.widget.textbox(),
}

local button = wibox.widget {
	icon,
	bg = bg_color,
	shape = helpers.rrect(22),
	forced_width = dpi(45),
	widget = wibox.container.background
}

-- Hover animation
button:connect_signal("mouse::enter", function ()
	button.bg = hover_color
end)
button:connect_signal("mouse::leave", function ()
	button.bg = bg_color
end)

helpers.add_hover_cursor(button, "hand1")

return button