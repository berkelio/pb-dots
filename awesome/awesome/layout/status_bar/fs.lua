local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local helpers = require('helpers')

local helpers = require("helpers")
local color = x.color2

local update_interval = 360 -- every 6 minutes

-- Use /dev/sdxY according to your setup
local disk_script = [[
    bash -c "
    	df -h /dev/nvme0n1p2 | tail -1 | awk '{print \$5}' | cut -d '%' -f1
    "
]]

local icon = wibox.widget {
	font = beautiful.widget_icon_font.." 12",
	markup = helpers.colorize_text("",x.foreground),
	valign = "center",
	widget = wibox.widget.textbox
}

local bar = wibox.widget {
	max_value = 100,
    value = 0,
    background_color = color.."90",
    border_width = dpi(0),
    color = color,
    shape = helpers.rrect(8),
    bar_shape = helpers.rrect(8),
    forced_height = dpi(5),
    forced_width = dpi(150),
    widget = wibox.widget.progressbar
}

local widget = wibox.widget {
	{
		icon,
		right = dpi(15),
		widget = wibox.container.margin
	},
	nil,
	{
		bar,
		top = dpi(12),
		bottom = dpi(12),
		widget = wibox.container.margin
	},
	layout = wibox.layout.align.horizontal
}


-- Periodically get disk space info
awful.widget.watch(disk_script, update_interval, function(_, stdout)
    bar.value = tonumber(stdout)
end)

return widget