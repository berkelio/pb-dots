local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local apps = require('config.apps')
local helpers = require('helpers')

local color = x.color4

local icon = wibox.widget {
	font = beautiful.widget_icon_font.." 12",
	valign = "center",
	widget = wibox.widget.textbox
}

local bar = wibox.widget {
	max_value = 100,
    value = 0,
    background_color = color.."90",
    border_width = dpi(0),
    color = color,
    shape = helpers.rrect(8),
    bar_shape = helpers.rrect(8),
    forced_height = dpi(5),
    forced_width = dpi(150),
    widget = wibox.widget.progressbar
}

local widget = wibox.widget {
	{
		icon,
		right = dpi(15),
		widget = wibox.container.margin
	},
	nil,
	{
		bar,
		top = dpi(12),
		bottom = dpi(12),
		widget = wibox.container.margin
	},
	layout = wibox.layout.align.horizontal
}


awesome.connect_signal("evil::volume", function(volume, mute)
	if mute == "off" then
		bar.value = 0
		icon.text = ""
	else
		bar.value = volume
		if volume > 100 then
			icon.markup = helpers.colorize_text("", x.foreground)
		elseif volume >= 50 then
			icon.markup = helpers.colorize_text("", x.foreground)
		elseif volume >= 25 then
			icon.markup = helpers.colorize_text("", x.foreground)
		elseif volume > 0 then
			icon.markup = helpers.colorize_text("", x.foreground)
		elseif volume == 0 then
			icon.markup = helpers.colorize_text("", x.foreground)
		end
	end
end)

return widget