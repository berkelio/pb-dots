local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")
--local pad = helpers.pad

--local start_symbol = ""
local start_symbol = "Apps"

local create_button = function (symbol, color, bg_color, hover_color)
    local widget = wibox.widget {
        font = "SF Pro Display Medium 8",
        align = "center",
        valign = "center",
        id = "text_role",
        markup = helpers.colorize_text(symbol, color),
        widget = wibox.widget.textbox(),
    }

    local section = wibox.widget {
        widget,
        bg = bg_color,
        forced_width = dpi(45),
        widget = wibox.container.background
        }
        

    -- Hover animation
    section:connect_signal("mouse::enter", function ()
        section.bg = hover_color
    end)
    section:connect_signal("mouse::leave", function ()
        section.bg = bg_color
    end)

   helpers.add_hover_cursor(section, "hand1")

    return section
end

local startmenu = create_button(start_symbol, x.color2, "#00000000", beautiful.hover_color)

local tooltip = awful.tooltip {};
tooltip.gaps = 10
tooltip.text = "Apps"
tooltip.margins_leftright = 10
tooltip.margins_topbottom = 10
tooltip.preferred_alignments = {"middle", "front", "back"}
tooltip.mode = "outside"
tooltip:add_to_object(startmenu)

startmenu:buttons(gears.table.join(
    awful.button({ }, 1, function ()
        awful.spawn.with_shell("~/.scripts/rofi-run launcher")
    end)
))

return startmenu