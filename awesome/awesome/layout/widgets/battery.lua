local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

--local keys = require("configuration.keys")
local helpers = require("helpers")

local battery = wibox.widget {
    text = "0",
    align  = 'center',
    valign = 'center',
    font = beautiful.font.." regular 7",
    widget = wibox.widget.textbox
}

local section = wibox.widget {
    battery,
    bg = "#00000000",
    forced_width = dpi(30),
    widget = wibox.container.background
}

section.visible = false

section:connect_signal("mouse::enter", function ()	section.bg = beautiful.hover_color end)
section:connect_signal("mouse::leave", function () section.bg = bg_color end)
helpers.add_hover_cursor(section, "hand1")

-- Creo el botón y que hace cada click. 
battery:buttons(gears.table.join(
    -- Left click - Mute / Unmute
    awful.button({ }, 1, function () awful.spawn.with_shell("/home/gonzalo/.scripts/nightmode") end),
    awful.button({ }, 3, function () helpers.run_or_raise({class = 'Power Manager'}, true, "xfce4-power-manager-settings") end),
    awful.button({ }, 4, function () awful.spawn.with_shell("light -A 10") end),
    awful.button({ }, 5, function () awful.spawn.with_shell("light -U 10") end)
))

local batt_tooltip = awful.tooltip {};
batt_tooltip.shape = helpers.rrect(6)
batt_tooltip.gaps = 10
batt_tooltip.text = "Hola, Gonzo"
batt_tooltip.margins_leftright = 10
batt_tooltip.margins_topbottom = 10
batt_tooltip.preferred_alignments = {"middle", "front", "back"}
batt_tooltip.mode = "outside"
batt_tooltip:add_to_object(battery)

awesome.connect_signal("evil::charger", function(plugged)
    if plugged then 
        section.visible = false
    else 
        section.visible = true
    end
end)

awesome.connect_signal("evil::battery", function(value)
    battery.text = value.."%"
    batt_tooltip.text = value.."%"
    if value > 20 then
        battery.markup = "<span color='"..x.color2.."'>"..value.."%</span>"
    elseif value > 10 then
        battery.markup = "<span color='"..x.color3.."'>"..value.."%</span>"
    else
        battery.markup = "<span color='"..x.color1.."'>"..value.."%</span>"
    end
end)

collectgarbage("step", 200) 

return section