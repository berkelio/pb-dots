local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")

--local volume_symbol = "vol"
--local muted_symbol = "vol"

local volume_symbol = ""
local muted_symbol = ""

local color_normal = x.foreground
local color_muted = x.foreground.."30"

local volume = helpers.create_button(volume_symbol, x.foreground, x.background, beautiful.hover_color )

volume:buttons(gears.table.join(
    awful.button({ }, 1, function () helpers.volume_control("toggle") end), -- Left click - Mute / Unmute   
    awful.button({ }, 3, function () helpers.run_or_raise({class = 'Pavucontrol'}, true, "pavucontrol") end), -- Right click - Run or raise pavucontrol
    awful.button({ }, 4, function () helpers.volume_control("up") end), -- Scroll - Increase / Decrease volume
    awful.button({ }, 5, function () helpers.volume_control("down") end) -- Scroll - Increase / Decrease volume
))


local vol_first_time = true
awesome.connect_signal("evil::volume", function(value, muted)
    local volume_icon = volume:get_all_children()[1]
    if vol_first_time then
        vol_first_time = false
    else
        if muted == "on" then
            volume_icon.markup = helpers.colorize_text(volume_symbol, color_normal)
        else
            volume_icon.markup = helpers.colorize_text(muted_symbol, color_muted)
        end
    end
end)

collectgarbage("step", 200) 

return volume
