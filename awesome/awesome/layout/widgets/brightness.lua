local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")

local bri_symbol = ""
local bri_color = x.color3

local bri_bar = wibox.widget {
    max_value     = 100,
    value         = 90,
    forced_height = 10,
    forced_width  = 50,
    color         = x.color3,
    background_color = "#00000030",
    visible       = false,
    bar_shape         = helpers.rrect(2),
    shape         = helpers.rrect(2),
    widget        = wibox.widget.progressbar,
}

local bri_icon = wibox.widget {
    font    = "Icomoon 8",
    align   = "center",
    valign  = "center",
    id      = "text_role",
    markup = helpers.colorize_text(bri_symbol, bri_color),
    widget = wibox.widget.textbox
}

local bri_bar_container = wibox.widget {
    bri_bar,
    visible = false,
    left = dpi(5),
    widget = wibox.container.margin
}
 

local widget = wibox.widget {
    {
        {
            bri_icon,
            left = dpi(8),
            widget = wibox.container.margin
        },
        {
            bri_bar_container,
            bottom = dpi(8),
            top = dpi(8),
            right = dpi(8),
            widget = wibox.container.margin

        },
        visible = true,
        spacing = dpi(0),
        layout = wibox.layout.fixed.horizontal,
    },
    bg = x.background,
    widget = wibox.container.background
}

widget:buttons(gears.table.join(
    awful.button({ }, 1, function () helpers.volume_control("toggle") end), -- Left click - Mute / Unmute   
    awful.button({ }, 3, function () helpers.run_or_raise({class = 'Pavucontrol'}, true, "pavucontrol") end), -- Right click - Run or raise pavucontrol
    awful.button({ }, 4, function () helpers.volume_control("up") end), -- Scroll - Increase / Decrease volume
    awful.button({ }, 5, function () helpers.volume_control("down") end) -- Scroll - Increase / Decrease volume
))

widget:connect_signal("mouse::enter", function ()
    widget.bg = beautiful.hover_color
end)
widget:connect_signal("mouse::leave", function ()
    widget.bg = bg_color
end)

helpers.add_hover_cursor(widget, "hand1")


local bri_timeout = gears.timer {
    timeout = 1.4,
    autostart = true,
    callback = function()
        bri_bar_container.visible = false
    end
}

local function toggle_pop()
    if bri_bar_container.visible then
        bri_timeout:again()
    else
        bri_bar_container.visible = true
        bri_timeout:start()
    end
end

local bri_first_time = true

awesome.connect_signal("evil::brightness", function(value, muted)
    if bri_first_time then
        bri_first_time = false
    else
        bri_bar.value = value
        bri_bar.visible = true
        toggle_pop()
    end
end)

collectgarbage("step", 200) 

return widget
