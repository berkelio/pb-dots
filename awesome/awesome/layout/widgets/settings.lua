local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")

local settings_symbol = ""

local color = x.foreground
local off_color = beautiful.widget_disabled_color

local bt = helpers.create_button(settings_symbol, color, "#00000000", beautiful.hover_color)

bt:buttons(gears.table.join(
    -- Left click - Mute / Unmute
    awful.button({ }, 1, function () cc_toggle(screen.primary) end),
    awful.button({ }, 3, function () awesome.emit_signal("toggle_tray") end)
))

local tooltip = awful.tooltip {};
tooltip.shape = helpers.rrect(6)
tooltip.gaps = 10
tooltip.text = "Settings"
tooltip.margins_leftright = 10
tooltip.margins_topbottom = 10
tooltip.preferred_alignments = {"middle", "front", "back"}
tooltip.mode = "outside"
tooltip:add_to_object(bt)

return bt