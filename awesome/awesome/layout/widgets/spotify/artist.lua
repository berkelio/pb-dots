local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local helpers = require("helpers")
local apps = require("config.apps")

local artist = wibox.widget {
    text    = "",
    valign  = 'center',
    align   = 'center',
    color   = x.color1,
    font    = "Sf Pro Display Bold 15",
    markup  = helpers.colorize_text("Radiohead", x.foreground),
    widget  = wibox.widget.textbox
}

local title = wibox.widget {
    text    = "",
    valign  = 'center',
    align   = 'center',
    color   = x.color1,
    font    = "Sf Pro Display 10",
    markup  = helpers.colorize_text("how to dissapear completely", x.foreground),
    widget  = wibox.widget.textbox
}

awesome.connect_signal("evil::spotify", function(artist, title, album, status)
    local a = helpers.pango_escape(artist)
    local t = helpers.pango_escape(title)
    artist.markup = helpers.colorize_text(a, x.color1)
end)

return artist