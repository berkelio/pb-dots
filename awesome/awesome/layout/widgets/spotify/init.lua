local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local helpers = require("helpers")
local apps = require("config.apps")
local symbol = ""
local color = x.color5
local widget_font = "Fira Sans 7"

local musicbox = wibox {
	width = dpi(380),
	height = dpi(150),
	bg = "#00000090",
	ontop = true,
	visible = false,
	class = "splash",
	shape = helpers.rrect(12)
}

local artist = wibox.widget {
    text    = "",
    valign  = 'center',
    align   = 'center',
    color   = x.color1,
    font    = "Sf Pro Display Bold 15",
    markup  = helpers.colorize_text("Radiohead", x.foreground),
    widget  = wibox.widget.textbox
}

local title = wibox.widget {
    text    = "",
    valign  = 'center',
    align   = 'center',
    color   = x.color1,
    font    = "Sf Pro Display 10",
    markup  = helpers.colorize_text("how to dissapear completely", x.foreground),
    widget  = wibox.widget.textbox
}

local album_cover = wibox.widget {
    clip_shape = helpers.rrect(dpi(8)),
    image = "/home/gonzaloaresrivas/.cache/covers/current.png",
    resize = true,
    forced_width = dpi(130),
    forced_height = dpi(130),
    widget = wibox.widget.imagebox
}

musicbox:setup {
	{
        {
            album_cover,
            margins = dpi(10),
            widget  = wibox.container.margin
        },
		nil,
        {
            artist,
            nil,
            title,
            layout  = wibox.layout.fixed.vertical,
        },
		layout  = wibox.layout.align.horizontal,
	},
	valign = "top",
	layout = wibox.container.place,
}

local spotify = helpers.create_button(symbol, x.foreground, "#00000000", beautiful.hover_color)

spotify:buttons(gears.table.join(
    -- Left click - Mute / Unmute
    awful.button({ }, 1, function () awful.spawn.with_shell ("playerctl -p spotify play-pause") end),
    awful.button({ }, 3, function () awesome.emit_signal("toggle_musicbox") end)
    --awful.button({ }, 3, function () helpers.run_or_raise({class = 'Spotify'}, false, user.music, { switchtotag = false}) end)
))

local tooltip = awful.tooltip {};
tooltip.shape = helpers.rrect(12)
tooltip.gaps = 10
tooltip.text = ""
tooltip.margins_leftright = 10
tooltip.margins_topbottom = 10
tooltip.preferred_alignments = {"middle", "front", "back"}
tooltip.mode = "outside"
tooltip:add_to_object(spotify)

spotify.visible = false

awesome.connect_signal("evil::spotify", function(artist, title, album, status)
    local a = helpers.pango_escape(artist)
    local t = helpers.pango_escape(title)
    local artist = musicbox:get_all_children()[1]
    --local title = spotify:get_all_children()[3]
    local spt = spotify:get_all_children()[1]
    if status == "playing" then
        tooltip.markup = "<span font='"..beautiful.font.." bold 10' color='"..x.color4.."'>"..a.."</span> - "..t
        artist.markup = helpers.colorize_text(a, x.color1)
        spt.markup = helpers.colorize_text(symbol, x.color2)
        spotify.visible = true
    elseif status == "paused" then
        tooltip.markup = "<span font='"..beautiful.font.." bold' color='"..x.foreground.."'>"..a.."</span> - "..t

        spt.markup = helpers.colorize_text(symbol, x.foreground)
        spotify.visible = true
    end
end)

local interval = 3
local script = [[
    sh -c 'pgrep spotify > /dev/null && echo on || echo "off"'
]]

awful.widget.watch(script, interval, function(widget, stdout)
    if stdout:match("off") then
        spotify.visible = false
    end
end)

awesome.connect_signal("toggle_musicbox", function()
	musicbox.visible = not musicbox.visible
	
	awful.placement.bottom_right(
		musicbox, 
		{
			margins = { 
				bottom = dpi(30), 
				right = dpi(10)
			}, 
			parent = awful.screen.focused()
		}
	)
end)

collectgarbage("step", 200) 

return spotify