local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

--local keys = require("config.keys")
local helpers = require("helpers")
clock_box = wibox({visible = true, ontop = false, type = "splash", width = dpi(50), height = dpi(20), x=0, y=0})
awful.placement.bottom_right(clock_box)
clock_box.bg = x.background.."00"
local widget_font = "Fira Sans bold 17"

local time = {
    {
        font = "Fira Sans Bold 12",
        color = x.color1,
        align = "right",
        valign = "center",
        widget = wibox.widget.textclock(helpers.colorize_text("%H:%M", x.foreground.."dd"))
    },
    {
        font = "Fira Sans bold 1",
        widget = wibox.widget.textclock("")
    },
    spacing = dpi(0),
    layout = wibox.layout.fixed.horizontal
}

clock_box:setup {
    -- Horizontal centering
    nil,
    {
        -- Vertical centering
        nil,
        {
            {
                {
                    {
                        time,
                        layout = wibox.layout.stack
                    },
                    {
                        nil,
                        nil,
                        expand = "none",
                        layout = wibox.layout.align.horizontal
                    },
                    spacing = dpi(0),
                    layout = wibox.layout.fixed.vertical
                },
                spacing = dpi(0),
                layout = wibox.layout.fixed.vertical

            },
            bottom = dpi(0),
            widget = wibox.container.margin
        },
        expand = "none",
        layout = wibox.layout.align.vertical
    },
    expand = "none",
    layout = wibox.layout.align.horizontal
}