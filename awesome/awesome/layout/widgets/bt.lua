local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")

local icon_bt = ""
local icon_connected = ""
local icon_apagado = ""

local bt_color = x.color1
local off_color = beautiful.widget_disabled_color
local connected_color = x.color6

local bt = helpers.create_button(icon_bt, bt_color, "#00000000", beautiful.hover_color)

bt:buttons(gears.table.join(
    -- Left click - Mute / Unmute
    awful.button({ }, 1, function () awful.spawn.raise_or_spawn ("~/.scripts/rofi-wifi-menu.sh") end),
    awful.button({ }, 3, function () awful.spawn.with_shell("kitty -c vpn -e ~/.scripts/vpndtv", {floating = true}) end)
))

local tooltip = awful.tooltip {};
tooltip.shape = helpers.rrect(6)
tooltip.gaps = 10
tooltip.text = "Conectado vía BT"
tooltip.margins_leftright = 10
tooltip.margins_topbottom = 10
tooltip.preferred_alignments = {"middle", "front", "back"}
tooltip.mode = "outside"
tooltip:add_to_object(bt)

local bt_script = [[
    bash -c '
        ~/.config/awesome/helpers/bt.sh
    ']]

local name_script = [[
    bash -c '
        echo info | bluetoothctl | grep -i name | cut -d ":" -f2
    ']] 

awful.widget.watch(bt_script, 3, function(widget, out)
    local t = bt:get_all_children()[1]
    if out:match('conectado') then        
        t.markup = helpers.colorize_text(icon_connected, x.color6)
        tooltip.text = "Conectado"
    elseif out:match('encendido') then
        t.markup = helpers.colorize_text(icon_bt, x.foreground)
        tooltip.text = "Bluethooth encendido"
    else
        t.markup = helpers.colorize_text(icon_bt, x.foreground.."30")
        tooltip.text = "Bluethooth encendido"
    end
    collectgarbage("step", 200) 
end)

return bt