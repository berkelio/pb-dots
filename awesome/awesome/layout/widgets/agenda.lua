local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local helpers = require("helpers")

local apps = require("config.apps")

local get_events = [[
    bash -c "gcalcli --calendar gonzalo.ares@uala.com.ar agenda --tsv | awk '{print $1\" \"$2\" \"$5\" \"$6\" \"$7\" \"$8\" \"$9\" \"$10\" \"$11\" \"$12\" \"$13}'"
]]

local agenda = wibox.widget {
    {
        markup = 'This <i>is</i> a <b>textbox</b>!!!',
        align  = 'center',
        valign = 'center',
        font   = beautiful.font.." 7",
        widget = wibox.widget.textbox
    },
    top = dpi(5),
    bottom = dpi(5),
    visible = true,
    widget = wibox.container.margin,
}

agenda:buttons(gears.table.join(
    -- Left click - Mute / Unmute
    awful.button({ }, 1, function () awful.spawn.with_shell ("playerctl -p ncspot,spotify play-pause") end),
    awful.button({ }, 3, function () helpers.run_or_raise({class = 'Spotify'}, false, user.music, { switchtotag = false}) end)
))

awful.widget.watch(get_events, 300, function(widget, stdout)  
    local w = agenda:get_all_children()[1]
    w.markup = stdout
end)

collectgarbage("step", 200) 

return agenda