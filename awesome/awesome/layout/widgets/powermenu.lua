local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")

local arch_icon = ""
--
local normal_color = x.color1

local create_button = function (symbol, color, bg_color, hover_color)
    local widget = wibox.widget {
        font = "Fira Code 7",
        align = "center",
        valign = "center",
        id = "text_role",
        markup = helpers.colorize_text(symbol, color),
        widget = wibox.widget.textbox(),
    }

    local section = wibox.widget {
        widget,
        bg = bg_color,
        forced_width = dpi(20),
        widget = wibox.container.background
        }
        

    -- Hover animation
    section:connect_signal("mouse::enter", function ()
        section.bg = hover_color
    end)
    section:connect_signal("mouse::leave", function ()
        section.bg = bg_color
    end)

   helpers.add_hover_cursor(section, "hand1")

    return section
end

local shutdown_button = create_button(arch_icon, normal_color, "#00000000", beautiful.hover_color)

shutdown_button:buttons(gears.table.join(
    awful.button({ }, 1, function() awful.spawn.with_shell("~/.scripts/rofi-powermenu") end),
    awful.button({ }, 3, function() awful.spawn.with_shell("~/.scripts/rofi-powermenu") end)
))

local tooltip = awful.tooltip {};
tooltip.gaps = 10
tooltip.text = "Apagar el sistema"
tooltip.margins_leftright = 10
tooltip.margins_topbottom = 10
tooltip.preferred_alignments = {"middle", "front", "back"}
tooltip.mode = "outside"
tooltip:add_to_object(shutdown_button)

return shutdown_button
