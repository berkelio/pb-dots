local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local helpers = require("helpers")
local apps = require("config.apps")

local clock_formats = {
    hour = '%H:%M',
    day  = '%d/%m/%Y',
}

local clock = wibox.widget {
    align  = "center",
    valign = "center",
    format = clock_formats.hour,
    font   = beautiful.font.." regular 8",
    widget = wibox.widget.textclock,
}

local date = wibox.widget {
    clock,
    fg = x.foreground,
    bg = "#00000000",
    forced_width = dpi(50),
    widget = wibox.container.background,
}

date:buttons(gears.table.join(
    -- Left click - Mute / Unmute
    awful.button({ }, 1, function () awesome.emit_signal("scratch::calendar") end)
))


local clock_tooltip = awful.tooltip {};
clock_tooltip.gaps = 10
clock_tooltip.margins_leftright = 10
clock_tooltip.margins_topbottom = 10
clock_tooltip.preferred_alignments = {"middle", "front", "back"}
clock_tooltip.mode = "outside"
clock_tooltip:add_to_object(date)

date:connect_signal('mouse::enter', function()
    clock_tooltip.text = os.date('%A, %d de %B de %Y')
end)

date:connect_signal("mouse::enter", function ()	date.bg = beautiful.hover_color end)
date:connect_signal("mouse::leave", function () date.bg = "#00000000" end)


return date