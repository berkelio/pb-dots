local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local helpers = require("helpers")

local network_symbol = ""
local wifi_symbol = ""
local disconneted_symbol = ""
local cable_symbol = ""
local vpn_symbol = ""

--local network_symbol = "net"
--local wifi_symbol = "wifi"
--local cable_symbol = "cable"
--local vpn_symbol = "vpn"

local network_color = x.color2
local disconneted_color = beautiful.widget_disabled_color

local network = helpers.create_button(network_symbol, network_color, "#00000000", beautiful.hover_color)
network.visible = false

network:buttons(gears.table.join(
    -- Left click - Mute / Unmute
    awful.button({ }, 1, function () awful.spawn.with_shell ("~/.scripts/rofi-wifi-menu.sh") end),
    awful.button({ }, 3, function () awful.spawn.with_shell("~/.scripts/rofi-vpn", {floating = true}) end)
))

local interval = 3
local script = [[
    sh -c '
    ~/.config/awesome/config/utils/network.sh
    '
]]

local tooltip = awful.tooltip {};
tooltip.shape = helpers.rrect(6)
tooltip.gaps = 10
tooltip.text = "Conectado a la VPN"
tooltip.margins_leftright = 10
tooltip.margins_topbottom = 10
tooltip.preferred_alignments = {"middle", "front", "back"}
tooltip.mode = "outside"
tooltip:add_to_object(network)
 
  -- Periodically get used space info
awful.widget.watch(script, interval, function(widget, stdout)
    local t = network:get_all_children()[1]
    if stdout:match('vpn') then
        t.markup = helpers.colorize_text(vpn_symbol, network_color)
        network.visible = true
    else 
        network.visible = true
    end
end)

collectgarbage("step", 200) 

return network