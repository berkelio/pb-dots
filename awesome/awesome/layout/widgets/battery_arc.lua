local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

--local keys = require("configuration.keys")
local helpers = require("helpers")

local charge_icon = wibox.widget{
    bg = x.color1.."00",
    widget = wibox.container.background,
    visible = false
}

local batt = wibox.widget{
    
}

awesome.connect_signal("signal::battery", function(value) 
    local fill_color = x.color2

    if value >= 11 and value <= 30 then
        fill_color = x.color3
    elseif value <= 10 then
        fill_color = x.color1
    end

    batt.colors = {fill_color}
    batt.value = value
end)

awesome.connect_signal("signal::charger", function(state)
    if state then
        charge_icon.visible = true
    else
        charge_icon.visible = false
    end
end)

local battery = wibox.widget {
    {
        charge_icon,
        colors = {x.color2},
        bg = x.color8 .. "00",
        value = 30,
        min_value = 0,
        max_value = 100,
        thickness = dpi(3),
        padding = dpi(3),
        -- rounded_edge = true,
        start_angle = math.pi * 3 / 2,
        widget = wibox.container.arcchart
    },
    top = dpi(6),
    bottom = dpi(6),
    visible = true,
    widget = wibox.container.margin,
}

local section = wibox.widget {
    battery,
    bg = "#00000000",
    forced_width = dpi(30),
    widget = wibox.container.background
}

section.visible = false

section:connect_signal("mouse::enter", function ()	section.bg = beautiful.hover_color end)
section:connect_signal("mouse::leave", function () section.bg = bg_color end)
helpers.add_hover_cursor(section, "hand1")

local plug_cmd = [[
    sh -c 
        'cat /sys/class/power_supply/BA*/status'
]]

-- Creo el botón y que hace cada click. 
battery:buttons(gears.table.join(
    -- Left click - Mute / Unmute
    awful.button({ }, 1, function () awful.spawn.with_shell("/home/gonzalo/.scripts/nightmode") end),
    awful.button({ }, 3, function () helpers.run_or_raise({class = 'Power Manager'}, true, "xfce4-power-manager-settings") end),
    awful.button({ }, 4, function () awful.spawn.with_shell("light -A 10") end),
    awful.button({ }, 5, function () awful.spawn.with_shell("light -U 10") end)
))


local batt_tooltip = awful.tooltip {};
batt_tooltip.shape = helpers.rrect(6)
batt_tooltip.gaps = 10
batt_tooltip.text = "Hola, Gonzo"
batt_tooltip.margins_leftright = 10
batt_tooltip.margins_topbottom = 10
batt_tooltip.preferred_alignments = {"middle", "front", "back"}
batt_tooltip.mode = "outside"
batt_tooltip:add_to_object(battery)

awesome.connect_signal("evil::charger", function(plugged)
    if plugged then 
        section.visible = false
    else 
        section.visible = true
    end
end)

awesome.connect_signal("evil::battery", function(value)
    local t = battery:get_all_children()[1]
    t.value = value
    batt_tooltip.text = value.."%"
    if value > 20 then
        t.colors = {x.color2}
    elseif value > 10 then
        t.colors = {x.color3}
    else
        t.colors = {x.color1}
    end
end)

collectgarbage("step", 200) 

return section