local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")


local microphone_symbol = ""
--local microphone_symbol = ""
--local nomicrophone_symbol = ""

--local microphone_symbol = ""
local nomicrophone_symbol = ""

local normal_color = x.foreground
local muted_color = x.foreground .. "30"

local microphone = helpers.create_button(microphone_symbol, normal_color, x.background, beautiful.hover_color)

microphone:buttons(gears.table.join(
    awful.button({ }, 1, function () awful.spawn.with_shell("pactl set-source-mute @DEFAULT_SOURCE@ toggle") end)
))

awesome.connect_signal("evil::microphone", function(muted)
    local icon = microphone:get_all_children()[1]
    if muted == "off" then
        icon.markup = helpers.colorize_text(nomicrophone_symbol, muted_color)
    else
        icon.markup = helpers.colorize_text(microphone_symbol, x.foreground)
    end
end)

return microphone
