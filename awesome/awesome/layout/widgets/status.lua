local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")

require('layout.status_bar')

local settings_symbol = ""

local settings_color = x.foreground
local off_color = beautiful.widget_disabled_color
local connected_color = x.color6

local settings = helpers.create_button(settings_symbol, settings_color, "#00000000", beautiful.hover_color)
settings.visible = true

settings:buttons(gears.table.join(
    -- Left click - Mute / Unmute
    awful.button({ }, 1, function () awesome.emit_signal("toggle_status") end),
    awful.button({ }, 3, function () awful.spawn.with_shell("kitty -c vpn -e ~/.scripts/vpndtv", {floating = true}) end)
))

local tooltip = awful.tooltip {};
tooltip.shape = helpers.rrect(6)
tooltip.gaps = 10
tooltip.text = "Configuración"
tooltip.margins_leftright = 10
tooltip.margins_topbottom = 10
tooltip.preferred_alignments = {"middle", "front", "back"}
tooltip.mode = "outside"
tooltip:add_to_object(settings)

return settings