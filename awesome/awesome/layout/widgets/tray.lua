local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")


screen.connect_signal("request::desktop_decoration", function(s)
    s.systray = wibox.widget.systray()
    s.traybox = wibox({ 
        screen = s,
        width = screen_width, 
        height = beautiful.wibar_height,
        visible = false, 
        type = "splash",
        ontop = true,
    })

end)

local traybox = wibox.widget{
    {
        {
            {
                widget = wibox.widget.systray,
            },
            top = dpi(4),
            bottom = dpi(4),
            left = dpi(5),
            right = dpi(5),
            widget = wibox.container.margin,
        },
        bg = "#111111",
        shape = helpers.rrect(dpi(8)),
        widget = wibox.container.background,
    },
    top = dpi(1),
    bottom = dpi(1),
    visible = false,
    widget = wibox.container.margin
}

local function enable_tray(c)
    local s = awful.screen.focused()
    if traybox.visible ~= true then
        traybox.visible = true
        --countdown_tray:start()
    else
        traybox.visible = false
        --countdown_tray:stop()
    end
end

awesome.connect_signal("toggle_tray", enable_tray)

return traybox
