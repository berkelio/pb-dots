local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local helpers = require("helpers")

local arch_symbol = ""
local color = x.color2
local arch = helpers.create_button(arch_symbol, color, "#00000000", beautiful.hover_color)

arch:buttons(gears.table.join(
    -- Left click - Mute / Unmute
    awful.button({ }, 1, function () awful.spawn.raise_or_spawn ("/home/gonzalo/.scripts/rofi-wifi-menu.sh") end),
    awful.button({ }, 3, function () awful.spawn.with_shell("kitty -c vpn -e /home/gonzalo/.scripts/vpndtv", {floating = true}) end)
))

awesome.connect_signal("evil::arch_updates", function(upds)
    if upds > 0 then
        arch.visible = true
    else
        arch.visible = false
    end
end)

collectgarbage("step", 200) 

return arch