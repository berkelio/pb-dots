local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local helpers = require("helpers")

local apps = require("config.apps")

local get_tasks = "bash -c \"task status:pending count\""

local tareas = helpers.create_button("", x.color6, "#00000000", beautiful.hover_color)

local tooltip = awful.tooltip {};
tooltip.gaps = 10
tooltip.text = "9 tareas pendientes"
tooltip.margins_leftright = 10
tooltip.margins_topbottom = 10
tooltip.preferred_alignments = {"middle", "front", "back"}
tooltip.mode = "outside"
tooltip:add_to_object(tareas)

tareas:connect_signal("mouse::enter", function () tareas.bg = beautiful.hover_color end)
tareas:connect_signal("mouse::leave", function () tareas.bg = "#00000000" end)

tareas:buttons(gears.table.join(
    -- Left click - Mute / Unmute
    awful.button({ }, 1, function () awful.spawn.with_shell ("~/.scripts/rofi-todo") end),
    awful.button({ }, 3, function () awful.spawn.with_shell("~/.scripts/rofi-addtask") end)
))

awesome.connect_signal("evil::tasks", function(num)
    numero = tostring(num)
    tooltip.text = "Tareas pendientes"
end)

awful.widget.watch(get_tasks, 10, function(widget, stdout)  
    numero = tostring(stdout)
    awesome.emit_signal("evil::tasks", numero)
end)

--emit_task_info()

collectgarbage("step", 200) 

return tareas