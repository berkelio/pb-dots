local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")

--local volume_symbol = "vol"
--local muted_symbol = "vol"

local volume_symbol = ""
local muted_symbol = ""

local bat_color = x.color2
local volume_muted_color = x.color8

-- battery widget
--------------------

-- battery icon
local bat_icon = wibox.widget{
    markup = helpers.colorize_text("", x.color2),
    font = "Material Icons 11",
    align = "center",
    valign = "center",
    widget = wibox.widget.textbox
}

-- battery progressbar
local battery_progress = wibox.widget{
	color				= x.color2,
	background_color	= "#00000000",
    forced_width        = dpi(30),
	--forced_height		= dpi(8),
    border_width        = dpi(1),
    border_color        = x.foreground .. "A6",
    paddings             = dpi(2),
    bar_shape           = helpers.rrect(dpi(2)),
	shape				= helpers.rrect(dpi(5)),
    value               = 70,
	max_value 			= 100,
    widget              = wibox.widget.progressbar,
}

-- battery half circle thing
local battery_border_thing = wibox.widget{
    {
        wibox.widget.textbox,
        widget = wibox.container.background,
        bg = x.foreground .. "A6",
        forced_width = dpi(7),
        forced_height = dpi(7),
        shape = function(cr, width, height)
            gears.shape.pie(cr,width, height, 0, math.pi)
        end
    },
    direction = "east",
    widget = wibox.container.rotate()
}

-- percentage
local bat_txt = wibox.widget{
    widget = wibox.widget.textbox,
    markup = "100%",
    font = "Material Icons Medium 11",
    valign = "center",
    align = "center"
}

-- init battery
local battery = wibox.widget{
    {
        {
            nil,
            {
                battery_progress,
                battery_border_thing,
                layout = wibox.layout.fixed.horizontal,
                spacing = dpi(-2)
            },
            layout = wibox.layout.fixed.horizontal,
            spacing = dpi(1)
        },
        left = dpi(4),
        right = dpi(4),
        bottom = dpi(4),
        top = dpi(4),
        widget = wibox.container.margin,
    },
    visible = false,
    widget = wibox.container.background,
}

battery:connect_signal("mouse::enter", function () battery.bg = beautiful.hover_color end)
battery:connect_signal("mouse::leave", function () battery.bg = x.background end)
battery:buttons(gears.table.join(
    -- Left click - Mute / Unmute
    awful.button({ }, 1, function () awful.spawn.with_shell("/home/garesriv/.scripts/nightmode") end),
    awful.button({ }, 3, function () helpers.run_or_raise({class = 'Power Manager'}, true, "xfce4-power-manager-settings") end),
    awful.button({ }, 4, function () awful.spawn.with_shell("light -A 10") end),
    awful.button({ }, 5, function () awful.spawn.with_shell("light -U 10") end)
))

awesome.connect_signal("evil::charger", function(plugged)
    if plugged then 
        battery.visible = false
    else 
        battery.visible = true
    end
end)

awesome.connect_signal("evil::battery", function(value)
    battery_progress.value = value
    --batt_tooltip.text = value.."%"
    if value > 20 then
        battery_progress.color = x.color2
    elseif value > 10 then
        battery_progress.color = x.color3
    else
        battery_progress.color = x.color1
    end
end)

collectgarbage("step", 200) 

return battery
