local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local helpers = require("helpers")
local apps = require("config.apps")
local symbol = ""
local color = x.color5
local widget_font = "Fira Sans 7"

local create_button = function (symbol, color, bg_color, hover_color)
    local widget = wibox.widget {
        font = beautiful.font_icon .. " 14",
        align = "center",
        valign = "center",
        id = "text_role",
        markup = helpers.colorize_text(symbol, color),
        widget = wibox.widget.textbox(),
    }

    local section = wibox.widget {
        widget,
        bg = bg_color,
        forced_width = dpi(30),
        widget = wibox.container.background
        }

    -- Hover animation
    section:connect_signal("mouse::enter", function ()
        section.bg = hover_color
    end)
    section:connect_signal("mouse::leave", function ()
        section.bg = bg_color
    end)

   helpers.add_hover_cursor(section, "hand1")

    return section
end

local musicbox = wibox {
	width = dpi(200),
	height = dpi(300),
	bg = x.background,
	ontop = true,
	visible = false,
	class = "splash",
	shape = helpers.rrect(12),
    border_width = dpi(1),
    border_color = "#000000"
}

local artist_name = wibox.widget {
    font = beautiful.font.." medium 12",
    align = 'center',
    valign = 'center',
    markup = "<span foreground='"..x.foreground.."'>Radiohead</span>",
    widget = wibox.widget.textbox
}

local album_cover = wibox.widget {
    clip_shape = helpers.rrect(dpi(8)),
    image = "/home/gonzaloaresrivas/.cache/covers/current.png",
    resize = true,
    forced_width = dpi(180),
    forced_height = dpi(180),
    widget = wibox.widget.imagebox
}

local song_title = wibox.widget {
    font = beautiful.font.." 9",
    align = 'center',
    valign = 'center',
    markup = "<span foreground='"..x.foreground.."'>Paranoid Android</span>",
    widget = wibox.widget.textbox,
}

local play_button = wibox.widget {
    font = beautiful.font_icon .. " 14",
    align = "center",
    valign = "center",
    id = "text_role",
    markup = helpers.colorize_text("", x.foreground),
    widget = wibox.widget.textbox(),
}
helpers.add_hover_cursor(play_button, "hand1")

local next = create_button("", x.foreground, "#00000000", beautiful.hover_color.."00")
local prev = create_button("", x.foreground, "#00000000", beautiful.hover_color.."00")

play_button:buttons(gears.table.join(awful.button({ }, 1, function () awful.spawn.with_shell("playerctl -p spotify play-pause") end)))
next:buttons(gears.table.join(awful.button({ }, 1, function () awful.spawn.with_shell("playerctl -p spotify next") end)))
prev:buttons(gears.table.join(awful.button({ }, 1, function () awful.spawn.with_shell("playerctl -p spotify previous") end)))

musicbox:setup {
	{
        {
            album_cover,
            margins = dpi(10),
            widget  = wibox.container.margin
        },
        {
            artist_name,
            nil,
            song_title,
            layout  = wibox.layout.fixed.vertical,
        },
        {
            {
                prev,
                play_button,
                next,
                layout = wibox.layout.align.horizontal
            },
            top = dpi(15),
            left = dpi(5),
            right = dpi(5),
            widget = wibox.container.margin,
        },           
		layout  = wibox.layout.fixed.vertical,
	},
	valign = "top",
	layout = wibox.container.place,
}

local spotify = helpers.create_button(symbol, x.foreground, "#00000000", beautiful.hover_color)

spotify:buttons(gears.table.join(
    -- Left click - Mute / Unmute
    awful.button({ }, 1, function () awful.spawn.with_shell ("playerctl -p spotify play-pause") end),
    awful.button({ }, 3, function () awesome.emit_signal("toggle_musicbox") end)
    --awful.button({ }, 3, function () helpers.run_or_raise({class = 'Spotify'}, false, user.music, { switchtotag = false}) end)
))

local tooltip = awful.tooltip {};
tooltip.shape = helpers.rrect(12)
tooltip.gaps = 10
tooltip.text = ""
tooltip.margins_leftright = 10
tooltip.margins_topbottom = 10
tooltip.preferred_alignments = {"middle", "front", "back"}
tooltip.mode = "outside"
tooltip:add_to_object(spotify)

spotify.visible = false

local interval = 3
local script = [[
    sh -c 'pgrep spotify > /dev/null && echo on || echo "off"'
]]

awful.widget.watch(script, interval, function(widget, stdout)
    if stdout:match("off") then
        spotify.visible = false
    end
end)

awesome.connect_signal("evil::spotify", function(artist, title, album, status, album_art)
    
    local cmd = string.format("curl -L -s %s -o %s", album_art, "/tmp/spotify.png")
    
    awful.spawn.easy_async_with_shell(cmd, function(stdout)
	    album_cover:set_image(gears.surface.load_uncached("/tmp/spotify.png"))
    end)

end)

awesome.connect_signal("evil::spotify", function(artist, title, album, status, album_art)

    local a = helpers.pango_escape(artist)
    local t = helpers.pango_escape(title)
    local spt = spotify:get_all_children()[1]

    if status == "playing" then
        tooltip.markup = "<span font='"..beautiful.font.." bold 10' color='"..x.color4.."'>"..a.."</span> - "..t
        artist_name.markup = helpers.colorize_text(a, x.color2)
        song_title.markup = helpers.colorize_text(t, x.foreground)
        spotify.visible = true
        spt.markup = helpers.colorize_text(symbol, x.color2)
        play_button:set_markup_silently(helpers.colorize_text("", x.foreground))
    elseif status == "paused" then
        tooltip.markup = "<span font='"..beautiful.font.." bold' color='"..x.foreground.."'>"..a.."</span> - "..t
        artist_name.markup = helpers.colorize_text(a, x.foreground)
        spotify.visible = true
        spt.markup = helpers.colorize_text(symbol, x.foreground)
        play_button:set_markup_silently(helpers.colorize_text("", x.foreground))
    end
end)

awesome.connect_signal("toggle_musicbox", function()
	musicbox.visible = not musicbox.visible
	
	awful.placement.top_right(
		musicbox, 
		{
			margins = { 
				top = beautiful.wibar_height+beautiful.useless_gap, 
				right = beautiful.useless_gap
			}, 
			parent = awful.screen.focused()
		}
	)
end)

collectgarbage("step", 200) 

return spotify