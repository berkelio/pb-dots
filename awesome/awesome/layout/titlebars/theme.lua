local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local helpers = require("helpers")
local keys = require("config.keys")
local decorations = require("layout.titlebars")

-- This decoration theme will round clients according to your theme's
-- border_radius value
-- Disable this if using `picom` to round your corners
--decorations.enable_rounding()

-- Button configuration
local gen_button_size = dpi(8)
local gen_button_margin = dpi(4)
local gen_button_color_unfocused = "#00000000"
local gen_button_shape = gears.shape.circle

local create_vertical_bar = function(c, pos, bg, size)

	-- Check if passed position is valid
	if (pos == 'top' or pos == 'bottom') then
		pos = 'left'
		bg = '#FF00FF'
	end 

	awful.titlebar(c, {position = pos, bg = bg, size = size}) : setup {
        {
            helpers.vertical_pad(gen_button_margin / 2),
            decorations.button(c, gen_button_shape, x.color1, gen_button_color_unfocused, x.color9, gen_button_size, gen_button_margin, "close"),
            decorations.button(c, gen_button_shape, x.color5, gen_button_color_unfocused, x.color12, gen_button_size, gen_button_margin, "maximize"),
            decorations.button(c, gen_button_shape, x.color3, gen_button_color_unfocused, x.color10, gen_button_size, gen_button_margin, "floating"),
            layout = wibox.layout.fixed.vertical
        },
        {
            --buttons = keys.titlebar_buttons,
            font = beautiful.titlebar_font,
            align = beautiful.titlebar_title_align or "center",
            widget = wibox.widget.textbox("")
        },
        {
            --decorations.button(c, gen_button_shape, x.color5, gen_button_color_unfocused, x.color13, gen_button_size, gen_button_margin, "floating"),
            helpers.vertical_pad(gen_button_margin / 2),
            layout = wibox.layout.fixed.vertical
        },
        layout = wibox.layout.align.vertical
    }
end

local create_horizontal_bar = function(c, pos, bg, size)

	-- Check if passed position is valid
	if (pos == 'left' or pos == 'right') then
		pos = 'top'
		bg = '#FF00FF'
	end 

	awful.titlebar(c, {position = pos, bg = bg, size = size}) : setup {
        {
            helpers.horizontal_pad(gen_button_margin / 2),
            --decorations.button(c, gen_button_shape, x.color5, gen_button_color_unfocused, x.color13, gen_button_size, gen_button_margin, "floating"),
            layout = wibox.layout.fixed.horizontal
        },
        {
            --buttons = keys.titlebar_buttons,
            font = beautiful.titlebar_font,
            align = beautiful.titlebar_title_align or "center",
            widget = beautiful.titlebar_title_enabled and awful.titlebar.widget.titlewidget(c) or wibox.widget.textbox("")
        },
        {
            decorations.button(c, gen_button_shape, x.color3, gen_button_color_unfocused, x.color11, gen_button_size, gen_button_margin, "floating"),
            decorations.button(c, gen_button_shape, x.color2, gen_button_color_unfocused, x.color12, gen_button_size, gen_button_margin, "maximize"),
            decorations.button(c, gen_button_shape, x.color1, gen_button_color_unfocused, x.color9, gen_button_size, gen_button_margin, "close"),
            helpers.horizontal_pad(gen_button_margin / 2),
            layout = wibox.layout.fixed.horizontal
        },
        layout = wibox.layout.align.horizontal
    }
end

local create_vertical_bar_dialog = function(c, pos, bg, size)

	-- Check if passed position is valid
	if (pos == 'top' or pos == 'bottom') then
		pos = 'left'
		bg = '#FF00FF'
	end 

	awful.titlebar(c, {position = pos, bg = bg, size = size}) : setup {
        {
            helpers.vertical_pad(gen_button_margin / 2),
            decorations.button(c, gen_button_shape, x.color1, gen_button_color_unfocused, x.color10, gen_button_size, gen_button_margin, "close"),
            layout = wibox.layout.fixed.vertical
        },
        {
            --buttons = keys.titlebar_buttons,
            font = beautiful.titlebar_font,
            align = beautiful.titlebar_title_align or "center",
            widget = wibox.widget.textbox("")
        },
        {
            decorations.button(c, gen_button_shape, x.color2, gen_button_color_unfocused, x.color10, gen_button_size, gen_button_margin, "floating"),
            decorations.button(c, gen_button_shape, x.color4, gen_button_color_unfocused, x.color12, gen_button_size, gen_button_margin, "maximize"),

            helpers.vertical_pad(gen_button_margin / 2),
            layout = wibox.layout.fixed.vertical
        },
        layout = wibox.layout.align.vertical
	}
end

local create_horizontal_bar_dialog = function(c, pos, bg, size)

	-- Check if passed position is valid
	if (pos == 'left' or pos == 'right') then
		pos = 'top'
		bg = '#FF00FF'
	end 

	awful.titlebar(c, {position = pos, bg = bg, size = size}) : setup {
        {
            helpers.horizontal_pad(gen_button_margin / 2),
            decorations.button(c, gen_button_shape, x.color1, gen_button_color_unfocused, x.color10, gen_button_size, gen_button_margin, "close"),
            layout = wibox.layout.fixed.horizontal
        },
        {
            --buttons = keys.titlebar_buttons,
            font = beautiful.titlebar_font,
            align = beautiful.titlebar_title_align or "center",
            widget = beautiful.titlebar_title_enabled and awful.titlebar.widget.titlewidget(c) or wibox.widget.textbox("")
        },
        {
            decorations.button(c, gen_button_shape, x.color2, gen_button_color_unfocused, x.color10, gen_button_size, gen_button_margin, "floating"),
            decorations.button(c, gen_button_shape, x.color4, gen_button_color_unfocused, x.color12, gen_button_size, gen_button_margin, "maximize"),
            helpers.horizontal_pad(gen_button_margin / 2),
            layout = wibox.layout.fixed.horizontal
        },
        layout = wibox.layout.align.horizontal
	}
end

local create_ncspot_bar = function(c, pos, bg, size)

	awful.titlebar(c, {position = pos, bg = bg, size = dpi(50)}) : setup {
        {
            decorations.text_button(c, "", "icomoon-feather", x.color10, x.color10, x.color1, 22, 10, "floating"),
            helpers.horizontal_pad(gen_button_margin / 2),
            --decorations.button(c, gen_button_shape, x.color5, gen_button_color_unfocused, x.color13, gen_button_size, gen_button_margin, "floating"),
            layout = wibox.layout.fixed.horizontal
        },
        {
            decorations.text_button(c, "", "icomoon", x.color10, x.color10, x.color1, 22, 10, "floating"),
            decorations.text_button(c, "", "icomoon", x.color10, x.color10, x.color1, 22, 10, "floating"),
            decorations.text_button(c, "", "icomoon", x.color10, x.color10, x.color1, 22, 10, "floating"),
            helpers.horizontal_pad(gen_button_margin / 2),
            layout = wibox.layout.fixed.horizontal
        },
        {
            decorations.text_button(c, "", "icomoon", x.color10, x.color10, x.color1, 22, 10, "floating"),
            decorations.text_button(c, "", "icomoon", x.color10, x.color10, x.color1, 22, 10, "floating"),
            decorations.text_button(c, "", "icomoon", x.color10, x.color10, x.color1, 22, 10, "floating"),
            helpers.horizontal_pad(gen_button_margin / 2),
            layout = wibox.layout.fixed.horizontal
        },
        layout = wibox.layout.align.horizontal
    }
end

-- Add a titlebar
client.connect_signal("request::titlebars", function(c)
    if c.type == 'normal' then
        if c.class == 'kitty' or c.class == 'bashtop' then
            create_horizontal_bar(c, 'top', x.background, beautiful.titlebar_size)
        elseif c.class == 'KeePassXC' then
            create_horizontal_bar(c, 'top', '#eff0f1', beautiful.titlebar_size)
        elseif c.class == 'Zathura' or c.class == "Brave-browser" or c.class == "firefox" then
            create_horizontal_bar(c, 'top', "#1c1e26", beautiful.titlebar_size)
        elseif c.class == 'discord' then
            create_horizontal_bar(c, 'top', "#242631", beautiful.titlebar_size)
        elseif c.class == "Todoist" then 
            create_horizontal_bar(c, 'top', "#282828", beautiful.titlebar_size)
        elseif c.class == "Gimp" then
            create_horizontal_bar(c, 'top', "#454545", beautiful.titlebar_size)
        elseif c.class == "Slack" then
            create_horizontal_bar(c, 'top', "#1a1c23", beautiful.titlebar_size)
        elseif c.class == "spad" then
            create_horizontal_bar(c, 'top', x.background.."b0", beautiful.titlebar_size)
        elseif c.class == "Spotify" then
            create_horizontal_bar(c, 'top', x.background, beautiful.titlebar_size)
        elseif c.class == "Emacs" then
            create_horizontal_bar(c, 'top', "#44475a", beautiful.titlebar_size)
        elseif c.class == "FortiClient" then
            create_horizontal_bar(c, 'top', "#0078bd", beautiful.titlebar_size)
        elseif c.class == "Code" then
            create_horizontal_bar(c, 'top', "#1c1e26", beautiful.titlebar_size)
        elseif c.class == "Logseq" then
            create_horizontal_bar(c, 'top', "#282a36", beautiful.titlebar_size)
        else 
            create_horizontal_bar(c, 'top', beautiful.gtk.get_theme_variables().bg_color, beautiful.titlebar_size)
            --create_horizontal_bar(c, 'top', "#3b4252", beautiful.titlebar_size)
        end

    elseif c.type == 'dialog' then

        if c.role == 'GtkFileChooserDialog' then
            create_horizontal_bar_dialog(c, 'top',beautiful.gtk.get_theme_variables().bg_color, beautiful.titlebar_size)

        elseif c.class == 'firefox' then
            create_horizontal_bar_dialog(c, 'top', beautiful.gtk.get_theme_variables().bg_color, beautiful.titlebar_size)

        elseif c.class == 'Gimp' then 
            create_horizontal_bar_dialog(c, 'top', "#484848", beautiful.titlebar_size)

        elseif c.class == 'Arandr' then
            create_vertical_bar(c, 'left',
                beautiful.gtk.get_theme_variables().bg_color, beautiful.titlebar_size)

        else
            create_horizontal_bar(c, 'top',
                beautiful.gtk.get_theme_variables().bg_color, beautiful.titlebar_size)
        end

    elseif c.type == 'modal' then
        create_vertical_bar(c, 'left', '#00000099', beautiful.titlebar_size)

    else
        create_vertical_bar(c, 'left', beautiful.background, beautiful.titlebar_size)
    end
end)
