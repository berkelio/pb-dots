local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local helpers = require("helpers")
local decorations = require("layout.titlebars")
local notifications = require("layout.notifications")
local beautiful = require("beautiful")

--local keys = require("configuration.keys")

local button_commands = {
    ['close'] = { fun = function(c) c:kill() end, track_property = nil } ,
    ['maximize'] = { fun = function(c) c.maximized = not c.maximized; c:raise() end, track_property = "maximized" },
    ['minimize'] = { fun = function(c) c.minimized = true end },
    ['sticky'] = { fun = function(c) c.sticky = not c.sticky; c:raise() end, track_property = "sticky" },
    ['ontop'] = { fun = function(c) c.ontop = not c.ontop; c:raise() end, track_property = "ontop" },
    ['floating'] = { fun = function(c) c.floating = not c.floating; c:raise() end, track_property = "floating" },
    ['next'] = { fun = function(c) awful.spawn('playerctl -p ncspot next', false) end, },
    ['toggle'] = { fun = function(c) awful.spawn('playerctl -p ncspot play-pause', false) end, },
    ['prev'] = { fun = function(c) awful.spawn('playerctl -p ncspot previous', false) end, },
}

local text_button = function (c, symbol, color, unfocused_color, hover_color, size, margin, cmd)
    local button = wibox.widget {
        align = "center",
        valign = "center",
        font = "icomoon-feather",
        -- Initialize with the "unfocused" color
        markup = helpers.colorize_text(symbol, unfocused_color),
        -- Increase the width of the textbox in order to make it easier to click. It does not affect the size of the symbol itself.
        forced_width = size + margin,
        widget = wibox.widget.textbox
    }

    button:buttons(gears.table.join(
        awful.button({ }, 1, function ()
            button_commands[cmd].fun(c)
        end)
    ))

    local p = button_commands[cmd].track_property
    -- Track client property if needed
    if p then
        c:connect_signal("property::"..p, function ()
            button.markup = helpers.colorize_text(symbol, c[p] and color .. "40" or color)
        end)
        c:connect_signal("focus", function ()
            button.markup = helpers.colorize_text(symbol, c[p] and color .. "40" or color)
        end)
        button:connect_signal("mouse::leave", function ()
        if c == client.focus then
                button.markup = helpers.colorize_text(symbol, c[p] and color .. "40" or color)
            else
                button.markup = helpers.colorize_text(symbol, unfocused_color)
            end
        end)
    else
        button:connect_signal("mouse::leave", function ()
            if c == client.focus then
                button.markup = helpers.colorize_text(symbol, color)
            else
                button.markup = helpers.colorize_text(symbol, unfocused_color)
            end
        end)
        c:connect_signal("focus", function ()
            button.markup = helpers.colorize_text(symbol, color)
        end)
    end
    button:connect_signal("mouse::enter", function ()
        button.markup = helpers.colorize_text(symbol, hover_color)
    end)
    c:connect_signal("unfocus", function ()
        button.markup = helpers.colorize_text(symbol, unfocused_color)
    end)

    return button
end

local toggle_button = wibox.widget {
    align = "center",
    valign = "center",
    font = "icomoon-feather",
    markup = helpers.colorize_text("G", x.color7),
    forced_width = 32,
    widget = wibox.widget.textbox
}

local artist_info = wibox.widget {
    font = beautiful.font.. " bold 12",
    valign = "center",
    text = "Blur",
    widget = wibox.widget.textbox
}

local song_info = wibox.widget {
    font = beautiful.font.. " 10",
    valign = "center",
    text = "Advert",
    widget = wibox.widget.textbox
}
--awesome.emit_signal("evil::spotify", artist, title, status)
awesome.connect_signal("evil::spotify", function(artist, title, status)
    artist_info.markup = artist
    song_info.markup = title
    if status == "playing" then
        toggle_button.markup = helpers.colorize_text("", x.color7)
    else
        toggle_button.markup = helpers.colorize_text("", x.color7)
    end
end)

local create_ncspot_bar = function(c, pos, bg, size)
	awful.titlebar(c, {position = "bottom", bg = x.background, size = dpi(80)}) : setup {
        {
            helpers.horizontal_pad(20),
            {
                {
                    artist_info,
                    song_info,
                    layout = wibox.layout.fixed.vertical,
                },
                top = dpi(15),
                widget = wibox.container.margin,
            },
            layout = wibox.layout.fixed.horizontal
        },
        {
            text_button(c, "", x.foreground, x.color2, x.color2, 22, 10, "prev"),
            toggle_button,
            text_button(c, "", x.foreground, x.color2, x.color2, 22, 10, "next"),
            layout = wibox.layout.fixed.horizontal
        },
        {
            text_button(c, "", x.color10, x.color10, x.color1, 22, 10, "floating"),
            text_button(c, "", "#FFFFFF", x.color1, x.color1, 20, 10, "floating"),
            helpers.horizontal_pad(20),
            layout = wibox.layout.fixed.horizontal
        },
        expand = "none",
        layout = wibox.layout.align.horizontal
    }
end

collectgarbage("step", 200) 

-- Add the titlebar whenever a new music client is spawned
table.insert(awful.rules.rules, {
    rule_any = {
        class = {
            "music",
        },
        instance = {
            "music",
        },
    },
    properties = {},
    callback = create_ncspot_bar
})
