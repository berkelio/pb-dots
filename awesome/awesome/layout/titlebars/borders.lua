local awful = require("awful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local beautiful = require("beautiful")

client.connect_signal("request::titlebars", function(c)
    local top_border = awful.titlebar(c, {size = beautiful.titlebar_border_size,position = "top"})
    local right_border = awful.titlebar(c, {size = beautiful.titlebar_border_size,position = "right"})
    local bottom_border = awful.titlebar(c, {size = beautiful.titlebar_border_size,position = "bottom"})
    local left_border = awful.titlebar(c, {size = beautiful.titlebar_border_size,position = "left"})
end)

screen.connect_signal("arrange", function (s)
    local max = s.selected_tag.layout.name == "max"
    local only_one = #s.tiled_clients == 1 -- use tiled_clients so that other floating windows don't affect the count
    -- but iterate over clients instead of tiled_clients as tiled_clients doesn't include maximized windows
    for _, c in pairs(s.clients) do
        if c.class == "term_scratchpad" then
            awful.titlebar.hide(c,"top")
            awful.titlebar.hide(c,"bottom")
            awful.titlebar.hide(c,"left")
            awful.titlebar.hide(c,"right")
        else
            if (max or only_one) and not c.floating or c.maximized then
                awful.titlebar.hide(c,"top")
                awful.titlebar.hide(c,"bottom")
                awful.titlebar.hide(c,"left")
                awful.titlebar.hide(c,"right")
            else
                awful.titlebar.show(c,"top")
                awful.titlebar.show(c,"bottom")
                awful.titlebar.show(c,"left")
                awful.titlebar.show(c,"right")
            end
        end
    end
end)