-- Standard awesome library
local gears = require("gears")
local awful = require("awful")

-- Theme handling library
local beautiful = require("beautiful")

-- Widget library
local wibox = require("wibox")

-- Helpers
local helpers = require("helpers")

icon_size = dpi(20)


-- Pop Up Notification
------------

local pop_icon = wibox.widget{
    forced_height = icon_size,
    forced_width = icon_size,
    font = "Material Icons 30",
    align = "center",
    valign = "center",
    widget = wibox.widget.textbox()
}

local pop_bar = wibox.widget {
    colors = {x.color4},
    bg = "#000000" .. "20",
    value = 30,
    min_value = 0,
    max_value = 100,
    thickness = dpi(7),
    padding = dpi(1),
    -- rounded_edge = true,
    start_angle = math.pi * 3 / 2,
    widget = wibox.container.arcchart,
}

local pop = wibox({
    type = "dock",
    screen = awful.screen.focused(),
    height = dpi(100),
    width = dpi(100),
    border_width = dpi(1),
    shape = helpers.rrect(8),
    border_color = "#000000",
    bg = x.background,
    ontop = true,
    visible = false
})

pop:setup {
    {
        {            
            {
                {
                    pop_bar,
                    --direction = 'east',
                    widget    = wibox.container.rotate,
                },
                margins = dpi(5),
                widget = wibox.container.margin
            },
            {
                pop_icon,
                widget = wibox.container.margin

            },
            spacing = dpi(5),
            layout = wibox.layout.stack
        },
        --margins = dpi(5),
        widget = wibox.container.margin
    },
    bg = x.background.."00",
    shape = helpers.rrect(12),
    widget = wibox.container.background
}
awful.placement.centered(pop)

local pop_timeout = gears.timer {
    timeout = 1.4,
    autostart = true,
    callback = function()
        pop.visible = false
    end
}

local function toggle_pop()
    if pop.visible then
        pop_timeout:again()
    else
        pop.visible = true
        pop_timeout:start()
    end
end

local vol_first_time = true
awesome.connect_signal("evil::volume", function(value, muted)
    if vol_first_time then
        vol_first_time = false
    else
        if (sidebar and sidebar.visible) or (client.focus and client.focus.class == "Pavucontrol") then
        else
            pop_icon.markup = helpers.colorize_text("", x.background)
            pop_bar.value = value
            pop_bar.visible = true
            pop.screen = awful.screen.focused()
            awful.placement.bottom(pop, {margins = {bottom = dpi(250)}})
            if muted == "on" then
                pop_icon.markup = helpers.colorize_text("", x.color4)
                pop_bar.color = x.color2
                pop_bar.bg = "#00000030"
                pop_bar.colors = {x.color4}
            else
                pop_icon.markup = helpers.colorize_text("", x.color4)
                pop_bar.color = x.color2
                pop_bar.bg = "#00000000"
                pop_bar.value = 0
            end

            toggle_pop()
        end
    end
end)

local bri_first_time = true
awesome.connect_signal("evil::brightness", function(value)
    if bri_first_time then
        bri_first_time = false
    else
        if (sidebar and sidebar.visible) then
        else
            pop.screen = awful.screen.focused(),
            awful.placement.bottom(pop, {margins = {bottom = dpi(250)}})
            pop_icon.markup = helpers.colorize_text("", x.color3)
            pop_bar.value = value
            pop_bar.colors = {x.color3}
            pop_bar.visible = true
        
            toggle_pop()
        end
    end
end)

local mic_first_time = true
awesome.connect_signal("evil::microphone", function (muted)
    if mic_first_time then
        mic_first_time = false
    else
        if (sidebar and sidebar.visible) or (client.focus and client.focus.class == "Pavucontrol") then
        else    
            pop.screen = awful.screen.focused(),
            awful.placement.bottom(pop, {margins = {bottom = dpi(250)}})
            pop_bar.visible = true
            
            if muted == "off" then
                pop_icon.markup = helpers.colorize_text("", x.color6)
                pop_bar.color = x.color4
                pop_bar.value = 0
            else
                pop_icon.markup = helpers.colorize_text("", x.color6)
                pop_bar.color = x.color6
                pop_bar.value = 100
            end
            
            toggle_pop()
        end
    end
end)

-- NIGHTMODE
awesome.connect_signal("evil::nightmode", function (muted)
    pop_icon.markup = helpers.colorize_text("", x.foreground)
    pop_bar.visible = false
    pop_icon.forced_height = dpi(200)
    pop_icon.forced_width = dpi(200)
    awful.placement.centered(pop, {margins = {top = dpi(600)}})

    if muted then
        pop_icon.markup = helpers.colorize_text("", x.foreground)
    else
        pop_icon.markup = helpers.colorize_text("", x.foreground)
    end

    toggle_pop()
end)

-- Layout list
-----------------

local layout_list = awful.widget.layoutlist {
    source = awful.widget.layoutlist.source.default_layouts, -- DOC_HIDE
    spacing = dpi(24),
    base_layout = wibox.widget {
        spacing = dpi(12),
        forced_num_cols = 4,
        layout = wibox.layout.grid.vertical
    },
    widget_template = {
        {
            {
                id = "icon_role",
                forced_height = dpi(68),
                forced_width = dpi(68),
                widget = wibox.widget.imagebox
            },
            margins = dpi(10),
            widget = wibox.container.margin
        },
        id = "background_role",
        forced_width = dpi(68),
        forced_height = dpi(68),
        widget = wibox.container.background
    }
}

local layout_popup = awful.popup {
    widget = wibox.widget {
        {layout_list, margins = dpi(30), widget = wibox.container.margin},
        bg = "#20212be0",
        forced_width = dpi(260),
        shape = helpers.rrect(8),
        border_color = "#000000",
        border_width = dpi(1),
        widget = wibox.container.background
    },
    placement = awful.placement.centered,
    ontop = true,
    visible = false,
    bg = x.background .. "00"
}

awful.placement.centered(layout_popup, {margins = {top = dpi(600)}})

function gears.table.iterate_value(t, value, step_size, filter, start_at)
    local k = gears.table.hasitem(t, value, true, start_at)
    if not k then return end

    step_size = step_size or 1
    local new_key = gears.math.cycle(#t, k + step_size)

    if filter and not filter(t[new_key]) then
        for i = 1, #t do
            local k2 = gears.math.cycle(#t, new_key + i)
            if filter(t[k2]) then return t[k2], k2 end
        end
        return
    end

    return t[new_key], new_key
end

awful.keygrabber {
    start_callback = function() layout_popup.visible = true end,
    stop_callback = function() layout_popup.visible = false end,
    export_keybindings = true,
    stop_event = "release",
    stop_key = {"Escape", "Super_L", "Super_R", "Mod4"},
    keybindings = {
        {
            {superkey, "Shift"}, " ", function()
                awful.layout.set(gears.table.iterate_value(layout_list.layouts,
                                                           layout_list.current_layout, -1),
                                 nil)
            end
        }, {
            {superkey}, " ", function()
                awful.layout.set(gears.table.iterate_value(layout_list.layouts,
                                                           layout_list.current_layout, 1),
                                 nil)
            end
        }
    }
}
