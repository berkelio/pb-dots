local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local keys = require("config.keys")
local helpers = require("helpers")

local bat_bar = wibox.widget {
    max_value     = 100,
    value         = 100,
    forced_height = dpi(2),
    forced_width  = 50,
    color         = x.color2,
    background_color = x.color8.."30",
    shape         = helpers.rrect(2),
    widget        = wibox.widget.progressbar,
}

screen.connect_signal("request::desktop_decoration", function(s)

    s.batline = wibox({ 
        screen = s,
        width = screen_width, 
        height = dpi(1),
        visible = true, 
        type = "splash",
        ontop = true,
        bg = x.color8.."00",
    })

    s.batline:setup{
        bat_bar,
        bg = "#00000000", 
        visible = true,
        widget = wibox.container.background,
    }

    awful.placement.bottom(s.batline)
end)

awesome.connect_signal("evil::charger", function(plugged)
    if plugged then 
        bat_bar.visible = false
    else 
        bat_bar.visible = true
    end
end)

awesome.connect_signal("evil::battery", function(value)
    bat_bar.value = value
    --batt_tooltip.text = value.."%"
    if value > 20 then
        bat_bar.color = x.color2
    elseif value > 10 then
        bat_bar.color = x.color3
    else
        bat_bar.color = x.color1
    end
end)

collectgarbage("step", 200) 

return batline
