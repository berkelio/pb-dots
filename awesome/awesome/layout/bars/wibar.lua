local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")
local menubar = require("menubar")
require('config.apps')
local helpers = require("helpers")
local modkey = "Mod4"

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

--screen.connect_signal("request::desktop_decoration", function(s)
awful.screen.connect_for_each_screen(function(s)
    --local tagnames = beautiful.tagnames or { "1", "2", "3", "4", "5", "6", "7", "8", "9"}
    --awful.tag(tagnames, s, awful.layout.layouts[1])

    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.noempty,
        layout   = {
            spacing = 0,
            layout  = wibox.layout.fixed.horizontal
        },
        widget_template = {
            {
                {
                    {
                        {
                            id     = 'text_role',
                            resize = true,
                            align = "center",
                            valign = "center",
                            widget = wibox.widget.textbox,
                        },
                        layout = wibox.layout.fixed.horizontal,
                    },
                    left = dpi(6),
                    right = dpi(6),
                    widget = wibox.container.margin
                },
                id     = 'background_role',
                widget = wibox.container.background,
            },
            top = dpi(0),
            bottom = dpi(0),
            widget = wibox.container.margin
        },
        buttons = {
            awful.button({ }, 1, function(t) t:view_only() end),
            awful.button({ modkey }, 1, function(t) if client.focus then client.focus:move_to_tag(t) end end),
            awful.button({ }, 3, awful.tag.viewtoggle), awful.button({ modkey }, 3, function(t) if client.focus then client.focus:toggle_tag(t) end end),
            awful.button({ }, 4, function(t) awful.tag.viewprev(t.screen) end),
            awful.button({ }, 5, function(t) awful.tag.viewnext(t.screen) end),
        },
    }
   
    -- Create the wibox
    s.mywibox = awful.wibar {
        position = beautiful.wibar_position,
        height = beautiful.wibar_height, 
        width = s.screen_width, 
        bg = beautiful.wibar_bg,
        visible = true,
        screen   = s,
        widget   = {
            { -- Left widgets
                require('layout.widgets.start_menu'),
                layout = wibox.layout.fixed.horizontal,
                spacing = dpi(5),
            },
            {
                s.mytaglist,
                top = dpi(3),
                bottom = dpi(3),
                widget = wibox.container.margin,
            },
                --require('layout.widgets.spotify'),
            { -- Right widgets                 
                --require('layout.widgets.volume'),
                --require('layout.widgets.mic'),
                --require('layout.widgets.brightness'),
                require('layout.widgets.bt'),
                require('layout.widgets.network'),                
                require('layout.widgets.battery'),
                --require('layout.widgets.spotify'),
                --require('layout.widgets.status'),
                --require('layout.widgets.settings'),
                require('layout.widgets.time'),
                require('layout.widgets.powermenu'),
                spacing = dpi(0),
                layout = wibox.layout.fixed.horizontal,
            },
            expand = "none",
            layout = wibox.layout.align.horizontal,
        }
    }

    --s.mywibox:struts({bottom=dpi(18)})

    awesome.connect_signal("toggle_wibar", function()
        s.mywibox.visible = not s.mywibox.visible
    end)    
    
end)
-- }}}

