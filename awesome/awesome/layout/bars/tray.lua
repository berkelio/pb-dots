local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

--local keys = require("configuration.keys")
local helpers = require("helpers")
--local pad = helpers.pad

screen.connect_signal("request::desktop_decoration", function(s)
        -- Create a system tray widget
        s.systray = wibox.widget.systray()
        s.traybox = wibox({ 
            screen = s,
            width = screen_width, 
            type = 'dock', 
            height = beautiful.traybox_height,
            visible = false, 
            ontop = true,
            bg = "#ff000000"
        })

        s.traybox:setup {
            {
                nul,
                layout = wibox.layout.fixed.horizontal,
            },
            {
                nul,
                spacing = dpi(2),
                layout = wibox.layout.fixed.horizontal
            },
            {    
                {
                    {
                        s.systray,
                        layout = wibox.layout.fixed.horizontal
                    },
                    right = dpi(10),
                    top = dpi(7),
                    bottom = dpi(7),
                    left = dpi(10),
                    widget = wibox.container.margin
                },
                bg = beautiful.bg_systray,
                border_color = "#000000",
                border_width = dpi(1),
                shape = helpers.rrect(8),
                widget = wibox.container.background,
            },
            expand = "none",
            layout = wibox.layout.align.horizontal
        }
    
        awful.placement.bottom_right(s.traybox, { margins = beautiful.useless_gap*2.5})
        s.traybox:buttons(gears.table.join(
        awful.button({ }, 2, function ()
            s.traybox.visible = false
        end)
    ))
    end)

local pop_timeout = gears.timer {
    timeout = 3,
    autostart = true,
    callback = function()
        local s = awful.screen.focused()
        s.traybox.visible = false
    end
}

local function toggle_tray()
    local s = awful.screen.focused()
    if s.traybox.visible then
        s.traybox.visible = false
    else
        s.traybox.visible = true
    end
end


awesome.connect_signal("toggle_tray", toggle_tray)