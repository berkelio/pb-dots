local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local keys = require("config.keys")
local helpers = require("helpers")

screen.connect_signal("request::desktop_decoration", function(s)
    s.taglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.noempty,
        layout   = {
            spacing = 0,
            layout  = wibox.layout.fixed.horizontal
        },
        widget_template = {
            {
                {
                    {
                        {
                            id     = 'text_role',
                            resize = true,
                            align = "center",
                            valign = "center",
                            widget = wibox.widget.textbox,
                        },
                        layout = wibox.layout.fixed.horizontal,
                    },
                    left = dpi(7),
                    right = dpi(7),
                    widget = wibox.container.margin
                },
                id     = 'background_role',
                shape = helpers.rrect(8), 
                widget = wibox.container.background,
            },
            top = dpi(4),
            bottom = dpi(4),
            widget = wibox.container.margin
        },
        buttons = {
            awful.button({ }, 1, function(t) t:view_only() end),
            awful.button({ modkey }, 1, function(t) if client.focus then client.focus:move_to_tag(t) end end),
            awful.button({ }, 3, awful.tag.viewtoggle), awful.button({ modkey }, 3, function(t) if client.focus then client.focus:toggle_tag(t) end end),
            awful.button({ }, 4, function(t) awful.tag.viewprev(t.screen) end),
            awful.button({ }, 5, function(t) awful.tag.viewnext(t.screen) end),
        },
    }

    s.tagbox = wibox({ 
        screen = s,
        width = screen_width, 
        height = beautiful.wibar_height*1.5,
        visible = false, 
        type = "splash",
        ontop = true,
        bg = "#99999900"
    })

    s.tagbox:setup{
        {
            nil,
            {
                {
                    s.taglist,
                    left = dpi(10),
                    right = dpi(10),
                    top = dpi(5),
                    bottom = dpi(5),
                    widget = wibox.container.margin,    
                },
                bg = x.background,
                border_color = "#000000",
                border_width = dpi(1),
                shape = helpers.rrect(8), 
                visible = true,
                widget = wibox.container.background,
            },
            nil,
            spacing = dpi(0),
            expand = "none",
            layout = wibox.layout.align.horizontal,
        },
        bg = "#00000000", 
        visible = true,
        widget = wibox.container.background,
    }

    awful.placement.bottom(s.tagbox, { margins = beautiful.useless_gap})
end)

local pop_timeout = gears.timer {
    timeout = 1.4,
    autostart = true,
    callback = function()
        local s = awful.screen.focused()
        s.tagbox.visible = false
    end
}

local function toggle_pop()
    local s = awful.screen.focused()
    if s.tagbox.visible then
        pop_timeout:again()
    else
        s.tagbox.visible = true
        pop_timeout:start()
    end
end

local function enable_tag(c)
    local s = awful.screen.focused()
    if s.tagbox.visible ~= true then
        s.tagbox.visible = true
        --countdown_tag:start()
    else
        s.tagbox.visible = false
        --countdown_tag:stop()
    end
end

awesome.connect_signal("toggle_tagbox", toggle_pop)
tag.connect_signal("property::selected", toggle_pop)
tag.connect_signal("property::urgent", toggle_pop)

   -- Get widgets

awful.screen.connect_for_each_screen(function(s)

    local tag_activator = wibox({
        y = 0, 
        height = 10,
        width = 500,
        visible = true, 
        ontop = false, 
        opacity = 0, 
        below = true, 
        screen = s
    })

    tag_activator:connect_signal("mouse::enter", function ()
        toggle_pop()
    end)

    awful.placement.bottom(tag_activator)

    tag_activator:buttons(
		gears.table.join(
			awful.button({ }, 4, function () awful.tag.viewprev() end),
			awful.button({ }, 5, function () awful.tag.viewnext() end)
	    )
    )
end)

return tagbox
