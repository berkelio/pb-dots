local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi

local json = require("helpers.json")

local OPEN_WEATHER_URL = "https://api.openweathermap.org/data/2.5/weather?id=3435910&units=metric&lang=es&appid="


local get_command = function(api_key)
    return "curl -X GET '"..OPEN_WEATHER_URL..api_key.."'"
end

local get_icon = function(condition)
    local icon

    if (condition == "Thunderstorm") then
        icon = "朗 "
    elseif (condition == "Drizzle") then
        icon = "殺 "
    elseif (condition == "Rain") then
        icon = "歹 "
    elseif (condition == "Snow") then
        icon = "流 "
    elseif (condition == "Clear") then
        local time = os.date("*t")
        if time.hour > 6 and time.hour < 18 then
            icon = "滛 "
        else
            icon = "望 "
        end
    elseif (condition == "Clouds") then
        icon = "摒 "
    else
        icon = "敖 "
    end

    return icon
end

local title = wibox.widget.textbox()
title.font = beautiful.font .. " 10"
title.align = 'left'
title.markup = "<span foreground='"..x.foreground.."55'>Buenos Aires</span>"

local temperature = wibox.widget {
    font = beautiful.font.." medium 10",
    markup = "<span foreground='"..x.foreground.."'>134 °C</span>",
    widget = wibox.widget.textbox
}

local description = wibox.widget {
    font = "Fira Mono 9", 
    markup = "<span foreground='"..x.foreground.."30'>Nublado</span>" ,
    widget = wibox.widget.textbox
}

local icon_widget = wibox.widget {
    font = "Weather Icons 30",
    align = "center",
    valign = "center",
    markup = "<span foreground='"..x.color6.."'>摒 </span>",
    widget = wibox.widget.textbox
}

local weather_widget = wibox.widget {
    {
        {
            {
                title,
                temperature,
                description,
                layout = wibox.layout.fixed.vertical,
            },
            nil,
            icon_widget,
            layout = wibox.layout.align.horizontal,
        },
        spacing = dpi(0),
        layout = wibox.layout.fixed.vertical,
    },
    margins = dpi(10),
    widget = wibox.container.margin,
}

local update_widget = function(widget, stdout, stderr)
    local result = json.decode(stdout)

    temperature.markup = "<span foreground='"..x.foreground.."'>"..tostring(result.main.temp).."°C</span> <span foreground='"..x.foreground.."30'>("..tostring(result.main.feels_like).."°C)</span>"
    description.markup = "<span foreground='"..x.foreground.."30'>"..result.weather[1].description.."</span>" 
    
    local condition = result.weather[1].main
    local icon = get_icon(condition)

    icon_widget.markup = "<span foreground='"..x.color6.."'>"..icon.."</span>"
end

local api_key_path = awful.util.getdir("config") .. "layout/sidebar/openweathermap.txt"
awful.spawn.easy_async_with_shell("cat "..api_key_path, function(stdout)
    local api_key = stdout:gsub("\n", "")

    -- wait for wifi to connect
    awful.spawn.easy_async_with_shell("sleep 10", function()
        awful.widget.watch(get_command(api_key), 5000, update_widget, weather_widget)
    end)
end)

return weather_widget