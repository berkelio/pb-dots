local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")

local clock_formats = {
    hour = '%H:%M',
    day  = '%A %d de %B, %Y',
}

local title = wibox.widget {
    widget = wibox.widget.textbox,
    font = beautiful.font .. " 10",
    align = 'left',
    markup = "<span foreground='"..x.foreground.."55'>TODO</span>"
}

local text = wibox.widget {
    widget = wibox.widget.textbox,
    font = beautiful.font .. " 10",
    align = 'left',
    markup = "<span foreground='"..x.foreground.."55'>Buenos Aires</span>"
}


local task_graph = wibox.widget {
    colors = {x.color2},
    bg = x.color2.."30",
    value = 1,
    min_value = 0,
    max_value = 100,
    thickness = dpi(8),
    padding = dpi(3),
    -- rounded_edge = true,
    start_angle = math.pi * 3 / 2,
    widget = wibox.container.arcchart
}

local widget = wibox.widget {
    {
        {
            title,
            fg = x.foreground,
            bg = "#00000000",
            widget = wibox.container.background,
        },
        {
            {
                text,
                fg = x.foreground,
                bg = "#00000000",
                widget = wibox.container.background,
            },
            nil,
            {
                task_graph,
                margins = dpi(7),
                widget = wibox.container.margin,
            }, 
            layout  = wibox.layout.align.horizontal,
        },
        layout  = wibox.layout.align.vertical,   
    },
    margins = dpi(10),
    widget = wibox.container.margin
}

awesome.connect_signal("evil::tasks", function(completas, pendientes, total)
    task_graph.max_value = total
    task_graph.value = completas
    text.markup = "<span color='"..x.color1.."'>Pendientes:</span> "..pendientes.."\n<span color='"..x.color2.."'>Completas:</span> "..completas
end)

return widget