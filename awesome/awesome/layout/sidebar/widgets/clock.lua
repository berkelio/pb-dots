local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")

local clock_formats = {
    hour = '%H:%M',
    day  = '%A %d de %B, %Y',
}

local clock = wibox.widget {
    align  = "center",
    valign = "center",
    format = clock_formats.hour,
    font   = beautiful.font.." Bold 25",
    widget = wibox.widget.textclock,
}

local fecha = wibox.widget {
    align  = "center",
    valign = "center",
    format = clock_formats.day,
    font   = beautiful.font.." 10",
    widget = wibox.widget.textclock,
}


local date = wibox.widget {
    {
        clock,
        {
            fecha,
            fg = x.foreground.."55",
            widget = wibox.container.background,        
        },
        layout  = wibox.layout.fixed.vertical    
    },
    fg = x.foreground,
    bg = "#00000000",
    widget = wibox.container.background,
}

local widget = wibox.widget {
    nil,
    date,
    nil,
    layout  = wibox.layout.fixed.horizontal
}

return date