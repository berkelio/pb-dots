local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")
--local pad = helpers.pad

local pfp = wibox.widget.imagebox()
pfp.image = "/home/gonzaloaresrivas/.config/awesome/yop.jpeg"
pfp.clip_shape = gears.shape.circle
pfp.forced_width = dpi(20)
pfp.forced_height = dpi(20)
pfp.border_width = dpi(3)
pfp.border_color = x.foreground

local user = wibox.widget.textbox()
user.font = beautiful.font.." 8"
user.align = 'left'
user.valign = "center"
user.markup = "<span foreground='"..x.foreground.."'>Gonza</span><span foreground='"..x.foreground.."50'>@Alderaan</span>"

local widget = wibox.widget {
    {
        pfp,
        top = dpi(10),
        widget = wibox.container.margin,
    },
    user,
    spacing = dpi(5),
    layout  = wibox.layout.fixed.horizontal
}

return widget