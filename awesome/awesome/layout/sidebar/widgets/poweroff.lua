local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local keys = require("config.keys")
local helpers = require("helpers")

local button_fg = x.foreground
local button_bg = "#66666620"
local hover_bg  = "#ffffff30"

local poweroff = helpers.create_sidebar_button("", x.foreground, x.color1, hover_bg, 10)

poweroff:buttons(gears.table.join(
    awful.button({ }, 1, function() awful.spawn.with_shell("~/.scripts/rofi-powermenu") end)
))

local widget = wibox.widget {
    poweroff,
    top=dpi(9),
    bottom=dpi(9),
    widget = wibox.container.margin,
}

return widget