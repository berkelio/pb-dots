local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")

-- battery icon
local bat_icon = wibox.widget{
    markup = helpers.colorize_text("", x.foreground),
    font = beautiful.font_icon .. " 10",
    align = "center",
    valign = "center",
    widget = wibox.widget.textbox
}

-- battery progressbar
local battery_progress = wibox.widget{
	color				= x.color2,
	background_color	= "#00000000",
    forced_width        = dpi(30),
    border_width        = dpi(1),
    border_color        = x.foreground,
    paddings             = dpi(2),
    bar_shape           = helpers.rrect(dpi(2)),
	shape				= helpers.rrect(dpi(5)),
    value               = 70,
	max_value 			= 100,
    widget              = wibox.widget.progressbar,
}

-- battery half circle thing
local battery_border_thing = wibox.widget{
    {
        wibox.widget.textbox,
        widget = wibox.container.background,
        bg = x.foreground,
        forced_width = dpi(8.2),
        forced_height = dpi(8.2),
        shape = function(cr, width, height)
            gears.shape.pie(cr,width, height, 0, math.pi)
        end
    },
    direction = "east",
    widget = wibox.container.rotate()
}

-- percentage
local bat_txt = wibox.widget{
    widget = wibox.widget.textbox,
    markup = "100%",
    font = beautiful.font .. " 8",
    valign = "center",
    align = "center"
}

-- init battery
local battery = wibox.widget{
    {
        nil,
        nil,
        {
            bat_txt,
            bat_icon,
            {
                {
                    {
                        battery_progress,
                        battery_border_thing,
                        layout = wibox.layout.fixed.horizontal,
                        spacing = dpi(-1.6)
                    },
                    layout = wibox.layout.stack,
                    spacing = dpi(0)
                },
                widget = wibox.container.margin,
                margins = {top = dpi(11),bottom = dpi(11)}
            },
            layout = wibox.layout.fixed.horizontal,
            spacing = dpi(3)
        },
        layout = wibox.layout.align.horizontal,
        spacing = dpi(3)
    },
    left = dpi(0),
    right = dpi(0),    
    widget = wibox.container.margin,
}

awesome.connect_signal("evil::battery", function(value)
    battery_progress.value = value
    bat_txt.markup = value .. "%"
    if value > 80 then
        battery_progress.color = x.color2
    elseif value > 10 then 
        battery_progress.color = x.color3
    else 
        battery_progress.color = x.color1
    end
end)

awesome.connect_signal("evil::charger", function(plugged)
    if plugged then 
        bat_icon.visible = true
        bat_txt.visible = false
    else 
        bat_icon.visible = false
        bat_txt.visible = true
    end
end)


return battery