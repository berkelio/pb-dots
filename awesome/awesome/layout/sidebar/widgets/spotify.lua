local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")
--local bling = require("modules.bling")
--local rubato = require("modules.rubato")

local playerctl = require("modules.bling").signal.playerctl.lib

local create_button = function (symbol, color, bg_color, hover_color)
    local widget = wibox.widget {
        font = beautiful.font_icon .. " 14",
        align = "center",
        valign = "center",
        id = "text_role",
        markup = helpers.colorize_text(symbol, color),
        widget = wibox.widget.textbox(),
    }

    local section = wibox.widget {
        widget,
        bg = bg_color,
        forced_width = dpi(30),
        widget = wibox.container.background
        }

    -- Hover animation
    section:connect_signal("mouse::enter", function ()
        section.bg = hover_color
    end)
    section:connect_signal("mouse::leave", function ()
        section.bg = bg_color
    end)

   helpers.add_hover_cursor(section, "hand1")

    return section
end

local cover = wibox.widget.imagebox()
cover.image = "/home/gonzaloaresrivas/.config/awesome/icons/spotify-client.svg"
cover.clip_shape = helpers.rrect(8)
cover.forced_width = dpi(80)
cover.forced_height = dpi(80)

local song_artist = wibox.widget.textbox()
song_artist.font = beautiful.font.." medium 12"
song_artist.align = 'center'
song_artist.valign = 'center'
song_artist.wrap = 'char'
song_artist.ellipsize  = 'end'
song_artist.forced_width = dpi(14)
song_artist.forced_height = dpi(16)
song_artist.markup = "<span foreground='"..x.foreground.."50'>Spotify</span>"

local song_title = wibox.widget.textbox()
song_title.font = beautiful.font.." 10"
song_title.align = 'center'
song_title.valign = 'center'
song_title.wrap = 'char'
song_title.ellipsize  = 'end'
song_title.forced_width = dpi(14)
song_title.forced_height = dpi(15)
song_title.markup = "<span foreground='"..x.foreground.."50'>Apagado</span>"

--local play_button = create_button("", x.foreground, "#00000000", beautiful.hover_color.."00")

local play_button = wibox.widget {
    font = beautiful.font_icon .. " 14",
    align = "center",
    valign = "center",
    id = "text_role",
    markup = helpers.colorize_text("", x.foreground),
    widget = wibox.widget.textbox(),
}
helpers.add_hover_cursor(play_button, "hand1")

local next = create_button("", x.foreground, "#00000000", beautiful.hover_color.."00")
local prev = create_button("", x.foreground, "#00000000", beautiful.hover_color.."00")

play_button:buttons(gears.table.join(
    awful.button({ }, 1, function ()
        awful.spawn.with_shell("playerctl -p spotify play-pause")
    end)
))

next:buttons(gears.table.join(
    awful.button({ }, 1, function ()
        awful.spawn.with_shell("playerctl -p spotify next")
    end)
))

prev:buttons(gears.table.join(
    awful.button({ }, 1, function ()
        awful.spawn.with_shell("playerctl -p spotify previous")
    end)
))

local widget = wibox.widget {
    {
        cover,   
        top = dpi(8),
        bottom = dpi(8),
        left = dpi(8),
        widget = wibox.container.margin,
    },
    {
        {
            song_artist,
            song_title,
            {
                {
                    prev,
                    play_button,
                    next,
                    layout = wibox.layout.align.horizontal
                },
                top = dpi(10),
                left = dpi(40),
                right = dpi(40),
                widget = wibox.container.margin,
            },           
            layout  = wibox.layout.fixed.vertical
        },
        top = dpi(10),
        bottom = dpi(10),
        widget = wibox.container.margin,
    },    
    expand = "inside",
    layout  = wibox.layout.align.horizontal
}
awesome.connect_signal("evil::spotify", function(artist, title, album, status, album_art)
    local a = helpers.pango_escape(artist)
    local t = helpers.pango_escape(title)
    
    song_artist:set_markup_silently(helpers.colorize_text(a, x.foreground))
    song_title:set_markup_silently(helpers.colorize_text(t, x.foreground))

    if status == "playing" then
        play_button:set_markup_silently(helpers.colorize_text("", x.foreground))
    elseif status == "paused" then
        play_button:set_markup_silently(helpers.colorize_text("", x.foreground))
    end
end)

awesome.connect_signal("evil::spotify", function(artist, title, album, status, album_art)
    local cmd = string.format("curl -L -s %s -o %s", album_art, "/tmp/spotify.png")
    
    awful.spawn.easy_async_with_shell(cmd, function(stdout)
	    cover:set_image(gears.surface.load_uncached("/tmp/spotify.png"))
    end)

end)

return widget