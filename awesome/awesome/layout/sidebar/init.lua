local awful = require("awful")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local helpers = require("helpers")
local wibox = require("wibox")
local gears = require("gears")
local rubato = require("modules.rubato")

local function box_widget(widgets, width, height, background)
	return wibox.widget {
		{
			{
				widgets,
				margins = dpi(1),
				widget = wibox.container.margin,
			},
			forced_width = dpi(width),
			forced_height = dpi(height),
			shape = helpers.rrect(8),
			fg = x.foreground,
			bg = background,
			widget = wibox.container.background,
		},
		margins = {left = dpi(10), right = dpi(10), bottom = dpi(10)},
		widget = wibox.container.margin,
	}
end

awful.screen.connect_for_each_screen(function(s)

	sidebar = wibox({
		width = dpi(310),
		height = dpi(640),
		bg = x.background,
		border_width = dpi(1),
		border_color = "#000000",
		ontop = true,
		visible = false,
		type = "splash",
		shape = helpers.rrect(12),
	})
		--~~~~~~~~~~~~~~~

		-- Get widgets
	local profile_widget = require('layout.sidebar.widgets.profile')
	local profile = box_widget(profile_widget, 110, 40, "#00000000")

	local poweroff_widget = require('layout.sidebar.widgets.poweroff')
	local poweroff = box_widget(poweroff_widget, 80, 40, "#00000000")

	local battery_widget = require('layout.sidebar.widgets.battery')
	local battery = box_widget(battery_widget, 1200, 40, "#00000000")

	local tasks_widget = require('layout.sidebar.widgets.tasks')
	local tasks = box_widget(tasks_widget, 1000, 90,"#00000030")
	
	local weather_widget = require('layout.sidebar.widgets.weather')
	local weather = box_widget(weather_widget, 1000, 70,"#00000030")

	local clock_widget = require('layout.sidebar.widgets.clock')
	local clock = box_widget(clock_widget, 360, 70, "#00000000")

	local monitors_widget = require('layout.sidebar.bars')
	local monitors = box_widget(monitors_widget, 340, 175, "#00000030")

	local spotify_widget = require('layout.sidebar.widgets.spotify')
	local spotify = box_widget(spotify_widget, 340, 85, "#00000030")

	local buttons_widget = require('layout.sidebar.buttons')
	local buttons = box_widget(buttons_widget, 260, 60, "#00000000")

	sidebar:setup {
		{
			{
				profile,
				battery,
				layout = wibox.layout.fixed.horizontal,
			},
			clock,
			weather,
			tasks,
			monitors,
			spotify,
			buttons,
			expand = "inside",
			layout = wibox.layout.fixed.vertical
		},
		valign = "top",
		layout = wibox.container.place
	}


		-- toggler script
		--~~~~~~~~~~~~~~~
		local screen_backup = 1

		cc_toggle = function(screen)
			sidebar.visible = not sidebar.visible

			awful.placement.bottom_right(
			sidebar, 
			{
				margins = beautiful.useless_gap*4, 
				parent = awful.screen.focused()
			}
		)

		end
		-- Eof toggler script
	--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	-- Get widgets


		local sidebar_activator = wibox({
			y = 0, 
			height = 10, 
			visible = true, 
			ontop = false, 
			opacity = 0, 
			below = true, 
			screen = s
		})
		sidebar_activator.width = sidebar.width

		sidebar_activator:connect_signal("mouse::enter", function ()
			cc_toggle()
		end)

		awful.placement.bottom_right(sidebar_activator)

		sidebar_activator:buttons(
			gears.table.join(
				awful.button({ }, 4, function ()
					awful.tag.viewprev()
				end),
				awful.button({ }, 5, function ()
					awful.tag.viewnext()
				end)
		))


end)