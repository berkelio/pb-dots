local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")

local volume = helpers.create_sidebar_bar("", x.foreground,x.color4)

volume:buttons(gears.table.join(
    awful.button({ }, 1, function() helpers.volume_control("toggle") end),
    awful.button({ }, 3, function() awful.spawn.with_shell("pavucontrol") end),
    awful.button({ }, 4, function() helpers.volume_control("up") end),
    awful.button({ }, 5, function() helpers.volume_control("down") end)
))

awesome.connect_signal("evil::volume", function(value, muted)
    local icon = volume:get_all_children()[1]
    local slider = volume:get_all_children()[3]
    if muted == "on" then
        slider.value = value
        icon.markup = helpers.colorize_text("", x.foreground)
        
    else
        slider.value = 0
        icon.markup = helpers.colorize_text("", x.foreground.."50")
    end
end)

return volume