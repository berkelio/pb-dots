local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")

local root = helpers.create_sidebar_bar("", x.foreground,x.color6)

awesome.connect_signal("evil::disk", function(value)
    local slider = root:get_all_children()[3]
    slider.value = value
end)

return root