local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")

tasks = helpers.create_sidebar_bar("", x.foreground,x.color1)

awesome.connect_signal("evil::tasks", function(completas, pendientes, total)
    local slider = tasks:get_all_children()[3]
    slider.value = completas
    slider.max_value = total
end)

return tasks