local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")
--local pad = helpers.pad

local title = wibox.widget.textbox()
title.font = beautiful.font .. " 10"
title.align = 'left'
title.markup = "<span foreground='"..x.foreground.."55'>Monitores</span>"

local vol = require("layout.sidebar.bars.vol")
local bri = require("layout.sidebar.bars.bri")
local cpu = require("layout.sidebar.bars.cpu")
local ram = require("layout.sidebar.bars.ram")
local root = require("layout.sidebar.bars.root")
local mic = require("layout.sidebar.bars.mic")
local tasks = require("layout.sidebar.bars.tasks")

local widget = wibox.widget {
    {
        --title,
        --tasks,
        vol,
        mic,
        bri,
        cpu,
        ram,
        root,
        spacing = dpi(10),
        layout  = wibox.layout.fixed.vertical
    },
    margins = dpi(10),
    widget = wibox.container.margin,
}

return widget