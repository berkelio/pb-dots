local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")

ram = helpers.create_sidebar_bar("", x.foreground,x.color5)

awesome.connect_signal("evil::ram", function(pct)
    local slider = ram:get_all_children()[3]
    slider.value = pct
end)

return ram