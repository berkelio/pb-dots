local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")

bri = helpers.create_sidebar_bar("", x.foreground,x.color3)

awesome.connect_signal("evil::brightness", function(value)
    local slider = bri:get_all_children()[3]
    slider.value = value
end)

bri:buttons(gears.table.join(
    awful.button({ }, 4, function() awful.spawn.with_shell("brightnessctl set 10+") end),
    awful.button({ }, 5, function() awful.spawn.with_shell("brightnessctl set 10-") end)
    
))
return bri