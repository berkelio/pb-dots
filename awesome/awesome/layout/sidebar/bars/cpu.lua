local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")

cpu = helpers.create_sidebar_bar("", x.foreground,x.color1)

awesome.connect_signal("evil::cpu", function(pct)
    local slider = cpu:get_all_children()[3]
    slider.value = pct
end)

return cpu