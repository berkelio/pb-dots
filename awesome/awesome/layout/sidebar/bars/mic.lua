local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local keys = require("config.keys")
local helpers = require("helpers")

local mic = helpers.create_sidebar_bar("", x.foreground,x.color2)

awesome.connect_signal("evil::microphone", function(muted)
    local icon = mic:get_all_children()[1]
    local slider = mic:get_all_children()[3]
    if muted == "on" then
        slider.value = 0
        icon.markup = helpers.colorize_text("", x.foreground.."50")
    else
        slider.value = 100
        icon.markup = helpers.colorize_text("", x.foreground)
    end
end)

mic:buttons(gears.table.join(
    awful.button({ }, 1, function () awful.spawn.with_shell("pactl set-source-mute @DEFAULT_SOURCE@ toggle") end)
))

return mic