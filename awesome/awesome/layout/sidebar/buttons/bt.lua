local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local keys = require("config.keys")
local helpers = require("helpers")

local button_fg = x.foreground
local button_bg = "#66666620"
local hover_bg  = "#ffffff30"

local bt = helpers.create_sidebar_button("",  button_fg, button_bg, hover_bg, 35)

local tooltip = awful.tooltip {};
tooltip.shape = helpers.rrect(6)
tooltip.gaps = 10
tooltip.margins_leftright = 10
tooltip.margins_topbottom = 10
tooltip.preferred_alignments = {"middle", "front", "back"}
tooltip.mode = "inside"
tooltip:add_to_object(bt)

local bt_script = [[
    bash -c '
        ~/.config/awesome/helpers/bt.sh
    ']]

awful.widget.watch(bt_script, 3, function(widget, out)
    local t = bt:get_all_children()[2]
    --local std = tostring(out:gsub("[^\n]*\n",""))
    if out:match('encendido') then        
        t.markup = helpers.colorize_text("", x.foreground)
        tooltip.text = "BT Encendido"
    elseif out:match('conectado - ') then        
        local t = bt:get_all_children()[2]
        local name = out:match('- (.*)\n')
        t.markup = helpers.colorize_text("", x.color4)
        tooltip.markup = "Conectado a <span color='"..x.color4.."'>"..name.."</span>"
    else 
        t.markup = helpers.colorize_text("", x.foreground.."30")
        tooltip.markup = "<span color='"..x.color1.."'>Apagado</span>"
    end
    collectgarbage("step", 200) 
end)

bt:buttons(gears.table.join(
    awful.button({ }, 1, function() awful.spawn.with_shell("~/.scripts/rofi-bt") end),
    awful.button({ }, 3, function() awful.spawn.with_shell("blueman-manager") end)
))

return bt