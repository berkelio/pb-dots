local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local keys = require("config.keys")
local helpers = require("helpers")

local button_fg = x.foreground
local button_bg = "#66666620"
local hover_bg  = "#ffffff30"

local net = helpers.create_sidebar_button("",  button_fg, button_bg, hover_bg, 35)

local tooltip = awful.tooltip {};
tooltip.shape = helpers.rrect(6)
tooltip.gaps = 10
tooltip.text = "Conectado vía BT"
tooltip.margins_leftright = 10
tooltip.margins_topbottom = 10
tooltip.preferred_alignments = {"middle", "front", "back"}
tooltip.mode = "inside"
tooltip:add_to_object(net)

local interval = 3
local script = [[
    sh -c '
        ~/.config/awesome/config/utils/network.sh
    '
]]
  -- Periodically get used space info
awful.widget.watch(script, interval, function(widget, stdout)
    local t = net:get_all_children()[2]
    local name = tostring(stdout:match('-(.*)\n'))
    if stdout:match('vpn - ') then
        t.markup = helpers.colorize_text("", button_fg)
        tooltip.markup = "Conectado a <span font='"..beautiful.font.." bold 10' color='"..x.color2.."'>"..name.."</span>"
    elseif stdout:match('wifi - ') then
        t.markup = helpers.colorize_text("", button_fg)
        tooltip.markup = "Conectado a <span font='"..beautiful.font.." bold 10' color='"..x.color2.."'>"..name.."</span>"
    elseif stdout:match('cable - ') then
        t.markup = helpers.colorize_text("", button_fg)
        tooltip.markup = "Conectado a <span font='"..beautiful.font.." bold 10' color='"..x.color2.."'>"..name.."</span>"
    else 
        t.markup = helpers.colorize_text("", button_fg)
        tooltip.markup = "<span font='"..beautiful.font.." bold 10' color='"..x.color1.."'>Desconectado</span>"
    end
end)

return net