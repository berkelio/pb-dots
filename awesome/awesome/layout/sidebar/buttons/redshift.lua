local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local keys = require("config.keys")
local helpers = require("helpers")

local button_fg = x.foreground
local button_bg = "#66666620"
local hover_bg  = "#ffffff30"

local redshift = helpers.create_sidebar_button("",  button_fg, button_bg, hover_bg, 35)

awesome.connect_signal("evil::redshift", function(stdout)
    local t = redshift:get_all_children()[2]
    if stdout:match('true') then
        t.markup = helpers.colorize_text("", x.color3)
    else 
        t.markup = helpers.colorize_text("", x.foreground)
    end
end)

local command = "pgrep redshift | wc -c"

awful.spawn.easy_async_with_shell(command, function(stdout)
    local t = redshift:get_all_children()[2]
    if stdout:match('0') then
        t.markup = helpers.colorize_text("", x.foreground)
    else 
        t.markup = helpers.colorize_text("", x.color3)
    end
end)

redshift:buttons(gears.table.join(
    awful.button({ }, 1, function() awful.spawn.with_shell("~/.scripts/nightmode") end),
    awful.button({ }, 3, function() awful.spawn.with_shell("~/.scripts/nightmode") end)
))

collectgarbage("step", 200) 

return redshift