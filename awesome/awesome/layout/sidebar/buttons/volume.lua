local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local keys = require("config.keys")
local helpers = require("helpers")

local button_fg = x.foreground
local button_bg = "#66666620"
local hover_bg  = "#ffffff30"

local vol = helpers.create_sidebar_button("",  button_fg, button_bg, hover_bg, 35)

awesome.connect_signal("evil::volume", function(value, muted)
    local t = vol:get_all_children()[2]
    local t_bg = vol:get_all_children()[1]
    if muted == "on" then
        t.markup = helpers.colorize_text("", button_fg)
    else
        t_bg.bg = x.color1
        t.markup = helpers.colorize_text("", x.color1)
    end
    --toggle_pop()
end)

return vol