local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local keys = require("config.keys")
local helpers = require("helpers")

local button_fg = x.foreground
local button_bg = x.color1
local hover_bg  = "#ffffff30"

local lock = helpers.create_sidebar_button("",  button_fg, button_bg, hover_bg, 35)

lock:buttons(gears.table.join(
    awful.button({ }, 1, function() awful.spawn.with_shell("~/.scripts/rofi-powermenu") end)
))

return lock