local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local keys = require("config.keys")
local helpers = require("helpers")

local button_fg = x.foreground
local button_bg = "#66666620"
local hover_bg  = "#ffffff30"

local mic = helpers.create_sidebar_button("",  button_fg, button_bg, hover_bg, 35)

awesome.connect_signal("evil::microphone", function(muted)
    local t = mic:get_all_children()[2]
    if muted == "off" then
        t.markup = helpers.colorize_text("", button_fg)
    else
        t.markup = helpers.colorize_text("", x.foreground)
    end
end)

return mic