local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local keys = require("config.keys")
local helpers = require("helpers")
local naughty = require("naughty")

local button_fg = x.foreground
local button_bg = "#66666620"
local hover_bg  = "#ffffff30"

local readwrite = require("helpers.readwrite")

local dnd = helpers.create_sidebar_button("",  button_fg, button_bg, hover_bg, 35)

local tooltip = awful.tooltip {};
tooltip.shape = helpers.rrect(6)
tooltip.gaps = 10
tooltip.margins_leftright = 10
tooltip.margins_topbottom = 10
tooltip.preferred_alignments = {"middle", "front", "back"}
tooltip.mode = "inside"
tooltip:add_to_object(dnd)


_G.awesome_dnd_state = false

local update_things = function ()
    local button = dnd:get_all_children()[2]
    if _G.awesome_dnd_state then
        button.markup = helpers.colorize_text("", x.color1)
        tooltip.text = "No Molestar"
    else
        button.markup = helpers.colorize_text("", x.foreground)
        tooltip.text = "Notificaciones activas"
    end 
end

-- reload old state
--~~~~~~~~~~~~~~~~~

local output = readwrite.readall("dnd_state")

local boolconverter={
    ["true"]  =   true,
    ["false"] =   false
}

awesome_dnd_state = boolconverter[output]
update_things()
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

dnd:buttons(gears.table.join(
            awful.button( {}, 1, function () 
                awesome_dnd_state = not awesome_dnd_state
                readwrite.write("dnd_state", tostring(_G.awesome_dnd_state))
                update_things()
            end)
    )
)

return dnd