local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local keys = require("config.keys")
local helpers = require("helpers")

local button_fg = x.foreground
local button_bg = "#66666620"
local hover_bg  = "#ffffff30"

local lock = helpers.create_sidebar_button("",  button_fg, button_bg, hover_bg, 35)

lock:buttons(gears.table.join(
    awful.button({ }, 1, function() awful.spawn.with_shell("betterlockscreen -l dim") end)
))

return lock