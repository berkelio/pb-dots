local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local keys = require("config.keys")
local helpers = require("helpers")

local create_button = function (symbol, color, bg_color, hover_color)
    local widget = wibox.widget {
        font = beautiful.font_icon .. " 12",
        align = "center",
        valign = "center",
        id = "text_role",
        markup = helpers.colorize_text(symbol, color),
        widget = wibox.widget.textbox(),
    }

    local section = wibox.widget {
        {
            widget,
            left = dpi(0),
            right = dpi(0),
            widget = wibox.container.margin
        },
        bg = bg_color,
        shape = gears.shape.circle,
        forced_width = dpi(40),
        forced_height = dpi(40),
        widget = wibox.container.background
        }

    section:connect_signal("mouse::enter", function () section.bg = hover_color end)
    section:connect_signal("mouse::leave", function () section.bg = bg_color end)
    helpers.add_hover_cursor(section, "hand1")
    return section
end

local button_fg = x.foreground
local button_bg = "#66666620"
local hover_bg  = "#ffffff30"

local net = require("layout.sidebar.buttons.net")
local mic = require("layout.sidebar.buttons.mic")
local vol = require("layout.sidebar.buttons.volume")
local bt = require("layout.sidebar.buttons.bt")
local dnd = require("layout.sidebar.buttons.dnd")
local night = require("layout.sidebar.buttons.redshift")
local lock = require("layout.sidebar.buttons.lock")
local poweroff = require("layout.sidebar.buttons.poweroff")

local widget = wibox.widget {
    nil,
    {
        net,
        bt,
        dnd,
        night,
        helpers.horizontal_pad(dpi(15)),
        lock,
        poweroff,
        spacing = dpi(10),
        expand = "none",
        layout  = wibox.layout.fixed.horizontal,
    },
    nil,
    expand = "outside",
    layout  = wibox.layout.align.vertical,
}

awesome.connect_signal("evil::volume", function(value, muted)
    local t = vol:get_all_children()[2]
    local t_bg = vol:get_all_children()[1]
    if muted == "on" then
        t.markup = helpers.colorize_text("", button_fg)
    else
        t_bg.bg = x.color1
        t.markup = helpers.colorize_text("", x.color1)
    end
    --toggle_pop()
end)

awesome.connect_signal("evil::microphone", function(muted)
    local t = mic:get_all_children()[2]
    if muted == "off" then
        t.markup = helpers.colorize_text("", button_fg)
    else
        t.markup = helpers.colorize_text("", x.foreground)
    end
end)

return widget